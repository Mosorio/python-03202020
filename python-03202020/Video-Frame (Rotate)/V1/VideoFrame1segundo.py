import time
import shutil
import os
from shutil import rmtree
from os import makedirs
from os import remove
# elimnar carpeta
#rmtree("Test")
#crear carpeta
#makedirs("Test/Frame")
# Mueve el archivo desde la ubicación actual a la
# carpeta "Documentos".
#shutil.move("archivo.txt", "Documentos/archivo.txt")

IntervaloFrame = 7
FrameActual = 1
TotalFramePorVideo = 9766

#Rutadestino = "Test/Frame"
RutaFrames  = "C:/AppServ/www/VideoFrame/Frame/"
Rutadestino = "C:/AppServ/www/VideoFrame/Captura/"
nombre_nuevo = Rutadestino  + "capture.jpg"

# ///////////////////////////////////////////////////
# Bloque Cargar y sobrescribir Frame
# ///////////////////////////////////////////////////

while FrameActual <= TotalFramePorVideo:

  if FrameActual < 9:

    FrameMove =  "scene0000" + str(FrameActual) + ".jpg"
    FileFrame = RutaFrames + FrameMove
    Frameactive = Rutadestino + FrameMove
    #print ("FrameMove (select): ", FrameMove)
    #print ("Ruta Take Frame: ", FileFrame)
    #print ("Ruta Destino: ", Rutadestino)
    #print ("Archivo: ", nombre_nuevo)
    # Remover el archivo Capture.jpg
    time.sleep(1)

    # Remueve el archivo del capture.jpg actual 
    while True:
      try:
        remove(nombre_nuevo)
        break
      except FileNotFoundError:
        print ("No se puede Encontrar el Archivo:", FileFrame)
      except PermissionError:
        print ("No se puede Encontrar el Archivo:", FileFrame)

    # copiar Frame Actual a la carpeta Frame (Todos los Frames del video scene00001, scene00002, scene00003 ...)
    while True:
      try:
        shutil.copy(FileFrame, Rutadestino)
        break
      except FileNotFoundError:
        print ("No se puede Encontrar Copiar:", FileFrame)


    # renombrar el Frame actual a "Capture.jpg", Esto para que el cliente pueda leer cuando se cambia de Frame
    while True:
      try:
        os.rename(Frameactive, nombre_nuevo)
        break
      except FileExistsError:
        print ("No se puede Encontrar Renombar:", FileFrame)

    # Imprir en Pantalla el nombre del Frame se copio, debe ir en order y en saltos de 7 scene00001 ---> scene00008
    print (FileFrame)

    # aumentamos el valor del Frame Actual, esto para leer el siguiente archivo o Frame disponible en la carpeta que tiene Todos los Frames
    FrameActual += IntervaloFrame
   
    # Esperamos unos milisegundos antes de pedir el otro Frame
    time.sleep(.200)
  
  elif FrameActual >= 10 and FrameActual < 100:

    FrameMove =  "scene000" + str(FrameActual) + ".jpg"
    FileFrame = RutaFrames + FrameMove
    Frameactive = Rutadestino + FrameMove
    #print ("FrameMove (select): ", FrameMove)
    #print ("Ruta Take Frame: ", FileFrame)
    #print ("Ruta Destino: ", Rutadestino)
    #print ("Archivo: ", nombre_nuevo)
    # Remover el archivo Capture.jpg
    time.sleep(1)

    # Remueve el archivo del capture.jpg actual 
    while True:
      try:
        remove(nombre_nuevo)
        break
      except FileNotFoundError:
        print ("No se puede Encontrar el Archivo:", FileFrame)
      except PermissionError:
        print ("No se puede Encontrar el Archivo:", FileFrame)

    # copiar Frame Actual a la carpeta Frame (Todos los Frames del video scene00001, scene00002, scene00003 ...)
    while True:
      try:
        shutil.copy(FileFrame, Rutadestino)
        break
      except FileNotFoundError:
        print ("No se puede Encontrar Copiar:", FileFrame)


    # renombrar el Frame actual a "Capture.jpg", Esto para que el cliente pueda leer cuando se cambia de Frame
    while True:
      try:
        os.rename(Frameactive, nombre_nuevo)
        break
      except FileExistsError:
        print ("No se puede Encontrar Renombar:", FileFrame)

    # Imprir en Pantalla el nombre del Frame se copio, debe ir en order y en saltos de 7 scene00001 ---> scene00008
    print (FileFrame)

    # aumentamos el valor del Frame Actual, esto para leer el siguiente archivo o Frame disponible en la carpeta que tiene Todos los Frames
    FrameActual += IntervaloFrame
   
    # Esperamos unos milisegundos antes de pedir el otro Frame
    time.sleep(.200)

  elif FrameActual >= 100 and FrameActual < 1000:

    FrameMove =  "scene00" + str(FrameActual) + ".jpg"
    FileFrame = RutaFrames + FrameMove
    Frameactive = Rutadestino + FrameMove
    #print ("FrameMove (select): ", FrameMove)
    #print ("Ruta Take Frame: ", FileFrame)
    #print ("Ruta Destino: ", Rutadestino)
    #print ("Archivo: ", nombre_nuevo)
    # Remover el archivo Capture.jpg
    time.sleep(1)

    # Remueve el archivo del capture.jpg actual 
    while True:
      try:
        remove(nombre_nuevo)
        break
      except FileNotFoundError:
        print ("No se puede Encontrar el Archivo:", FileFrame)
      except PermissionError:
        print ("No se puede Encontrar el Archivo:", FileFrame)

    # copiar Frame Actual a la carpeta Frame (Todos los Frames del video scene00001, scene00002, scene00003 ...)
    while True:
      try:
        shutil.copy(FileFrame, Rutadestino)
        break
      except FileNotFoundError:
        print ("No se puede Encontrar Copiar:", FileFrame)


    # renombrar el Frame actual a "Capture.jpg", Esto para que el cliente pueda leer cuando se cambia de Frame
    while True:
      try:
        os.rename(Frameactive, nombre_nuevo)
        break
      except FileExistsError:
        print ("No se puede Encontrar Renombar:", FileFrame)

    # Imprir en Pantalla el nombre del Frame se copio, debe ir en order y en saltos de 7 scene00001 ---> scene00008
    print (FileFrame)

    # aumentamos el valor del Frame Actual, esto para leer el siguiente archivo o Frame disponible en la carpeta que tiene Todos los Frames
    FrameActual += IntervaloFrame
   
    # Esperamos unos milisegundos antes de pedir el otro Frame
    time.sleep(.300)


  elif FrameActual >= 1000 and FrameActual < 10000:

    FrameMove =  "scene0" + str(FrameActual) + ".jpg"
    FileFrame = RutaFrames + FrameMove
    Frameactive = Rutadestino + FrameMove
    #print ("FrameMove (select): ", FrameMove)
    #print ("Ruta Take Frame: ", FileFrame)
    #print ("Ruta Destino: ", Rutadestino)
    #print ("Archivo: ", nombre_nuevo)
    # Remover el archivo Capture.jpg
    time.sleep(1)

    # Remueve el archivo del capture.jpg actual 
    while True:
      try:
        remove(nombre_nuevo)
        break
      except FileNotFoundError:
        print ("No se puede Encontrar el Archivo:", FileFrame)
      except PermissionError:
        print ("No se puede Encontrar el Archivo:", FileFrame)

    # copiar Frame Actual a la carpeta Frame (Todos los Frames del video scene00001, scene00002, scene00003 ...)
    while True:
      try:
        shutil.copy(FileFrame, Rutadestino)
        break
      except FileNotFoundError:
        print ("No se puede Encontrar Copiar:", FileFrame)


    # renombrar el Frame actual a "Capture.jpg", Esto para que el cliente pueda leer cuando se cambia de Frame
    while True:
      try:
        os.rename(Frameactive, nombre_nuevo)
        break
      except FileExistsError:
        print ("No se puede Encontrar Renombar:", FileFrame)

    # Imprir en Pantalla el nombre del Frame se copio, debe ir en order y en saltos de 7 scene00001 ---> scene00008
    print (FileFrame)

    # aumentamos el valor del Frame Actual, esto para leer el siguiente archivo o Frame disponible en la carpeta que tiene Todos los Frames
    FrameActual += IntervaloFrame
   
    # Esperamos unos milisegundos antes de pedir el otro Frame
    time.sleep(.300)

  else:
    FrameActual >= 10000 and FrameActual < 100000

    FrameMove =  "scene" + str(FrameActual) + ".jpg"
    FileFrame = RutaFrames + FrameMove
    Frameactive = Rutadestino + FrameMove
    #print ("FrameMove (select): ", FrameMove)
    #print ("Ruta Take Frame: ", FileFrame)
    #print ("Ruta Destino: ", Rutadestino)
    #print ("Archivo: ", nombre_nuevo)
    # Remover el archivo Capture.jpg
    time.sleep(1)

    # Remueve el archivo del capture.jpg actual 
    while True:
      try:
        remove(nombre_nuevo)
        break
      except FileNotFoundError:
        print ("No se puede Encontrar el Archivo:", FileFrame)
      except PermissionError:
        print ("No se puede Encontrar el Archivo:", FileFrame)

    # copiar Frame Actual a la carpeta Frame (Todos los Frames del video scene00001, scene00002, scene00003 ...)
    while True:
      try:
        shutil.copy(FileFrame, Rutadestino)
        break
      except FileNotFoundError:
        print ("No se puede Encontrar Copiar:", FileFrame)


    # renombrar el Frame actual a "Capture.jpg", Esto para que el cliente pueda leer cuando se cambia de Frame
    while True:
      try:
        os.rename(Frameactive, nombre_nuevo)
        break
      except FileExistsError:
        print ("No se puede Encontrar Renombar:", FileFrame)

    # Imprir en Pantalla el nombre del Frame se copio, debe ir en order y en saltos de 7 scene00001 ---> scene00008
    print (FileFrame)

    # aumentamos el valor del Frame Actual, esto para leer el siguiente archivo o Frame disponible en la carpeta que tiene Todos los Frames
    FrameActual += IntervaloFrame
   
    # Esperamos unos milisegundos antes de pedir el otro Frame
    time.sleep(.300)

print (FrameActual)
print ("Fin Task")

if FrameActual < TotalFramePorVideo:
  FrameActual = 1
  print ("Reset: ", FrameActual)
  print ("************ Reset ***************")

