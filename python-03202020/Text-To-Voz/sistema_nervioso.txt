El sistema nervioso es un conjunto organizado de células
especializadas en la conducción de señales eléctricas. La célula
básica del sistema nervioso de todos los animales es la neurona.