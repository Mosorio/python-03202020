-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 21-10-2019 a las 11:46:41
-- Versión del servidor: 5.7.17-log
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `tolltest`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `record`
--

CREATE TABLE `record` (
  `id` int(11) NOT NULL,
  `InfoLine` text NOT NULL,
  `TotalIdentificacion` int(10) NOT NULL,
  `NumeroTest` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `record`
--

INSERT INTO `record` (`id`, `InfoLine`, `TotalIdentificacion`, `NumeroTest`) VALUES
(1, '2019-10-16 16:32:12,510 [Thread:  26] INFO  [EDGE Detection] Identified Person >> Name: Chino Lyn Py Edge, PersonId: 6ce62b13-af64-4d43-9c01-2ab530ab30d7, GroupId: 684656de-0bec-4ad3-9dd5-1c7e5c0b7756, MatchProbability: 81.83%, FrameTimestamp: 10/16/2019 4:32:11 PM\n', 1, 32),
(2, '2019-10-16 16:32:14,576 [Thread:  16] INFO  [EDGE Detection] Identified Person >> Name: Chino Lyn Py Edge, PersonId: 6ce62b13-af64-4d43-9c01-2ab530ab30d7, GroupId: 684656de-0bec-4ad3-9dd5-1c7e5c0b7756, MatchProbability: 84.15%, FrameTimestamp: 10/16/2019 4:32:14 PM\n', 2, 32),
(3, '2019-10-16 16:32:17,652 [Thread:  33] INFO  [EDGE Detection] Identified Person >> Name: Chino Lyn Py Edge, PersonId: 6ce62b13-af64-4d43-9c01-2ab530ab30d7, GroupId: 684656de-0bec-4ad3-9dd5-1c7e5c0b7756, MatchProbability: 84.29%, FrameTimestamp: 10/16/2019 4:32:17 PM\n', 3, 32),
(4, '2019-10-16 16:32:20,531 [Thread:  16] INFO  [EDGE Detection] Identified Person >> Name: Chino Lyn Py Edge, PersonId: 6ce62b13-af64-4d43-9c01-2ab530ab30d7, GroupId: 684656de-0bec-4ad3-9dd5-1c7e5c0b7756, MatchProbability: 84.63%, FrameTimestamp: 10/16/2019 4:32:20 PM\n', 4, 32),
(5, '2019-10-16 16:32:23,616 [Thread:  16] INFO  [EDGE Detection] Identified Person >> Name: Chino Lyn Py Edge, PersonId: 6ce62b13-af64-4d43-9c01-2ab530ab30d7, GroupId: 684656de-0bec-4ad3-9dd5-1c7e5c0b7756, MatchProbability: 82.65%, FrameTimestamp: 10/16/2019 4:32:23 PM\n', 5, 32),
(6, '2019-10-16 16:32:26,469 [Thread:  26] INFO  [EDGE Detection] Identified Person >> Name: Chino Lyn Py Edge, PersonId: 6ce62b13-af64-4d43-9c01-2ab530ab30d7, GroupId: 684656de-0bec-4ad3-9dd5-1c7e5c0b7756, MatchProbability: 83.83%, FrameTimestamp: 10/16/2019 4:32:26 PM\n', 6, 32),
(7, '2019-10-16 16:32:29,524 [Thread:  26] INFO  [EDGE Detection] Identified Person >> Name: Chino Lyn Py Edge, PersonId: 6ce62b13-af64-4d43-9c01-2ab530ab30d7, GroupId: 684656de-0bec-4ad3-9dd5-1c7e5c0b7756, MatchProbability: 83.58%, FrameTimestamp: 10/16/2019 4:32:29 PM\n', 7, 32),
(8, '2019-10-16 16:32:32,566 [Thread:  26] INFO  [EDGE Detection] Identified Person >> Name: Chino Lyn Py Edge, PersonId: 6ce62b13-af64-4d43-9c01-2ab530ab30d7, GroupId: 684656de-0bec-4ad3-9dd5-1c7e5c0b7756, MatchProbability: 83.77%, FrameTimestamp: 10/16/2019 4:32:32 PM\n', 8, 32),
(9, '2019-10-16 16:32:35,621 [Thread:  26] INFO  [EDGE Detection] Identified Person >> Name: Chino Lyn Py Edge, PersonId: 6ce62b13-af64-4d43-9c01-2ab530ab30d7, GroupId: 684656de-0bec-4ad3-9dd5-1c7e5c0b7756, MatchProbability: 83.27%, FrameTimestamp: 10/16/2019 4:32:35 PM\n', 9, 32),
(10, '2019-10-16 16:32:38,475 [Thread:  29] INFO  [EDGE Detection] Identified Person >> Name: Chino Lyn Py Edge, PersonId: 6ce62b13-af64-4d43-9c01-2ab530ab30d7, GroupId: 684656de-0bec-4ad3-9dd5-1c7e5c0b7756, MatchProbability: 83.88%, FrameTimestamp: 10/16/2019 4:32:38 PM\n', 10, 32),
(11, '2019-10-16 16:32:41,536 [Thread:  26] INFO  [EDGE Detection] Identified Person >> Name: Chino Lyn Py Edge, PersonId: 6ce62b13-af64-4d43-9c01-2ab530ab30d7, GroupId: 684656de-0bec-4ad3-9dd5-1c7e5c0b7756, MatchProbability: 84.17%, FrameTimestamp: 10/16/2019 4:32:41 PM\n', 11, 32),
(12, '2019-10-16 16:32:44,587 [Thread:  26] INFO  [EDGE Detection] Identified Person >> Name: Chino Lyn Py Edge, PersonId: 6ce62b13-af64-4d43-9c01-2ab530ab30d7, GroupId: 684656de-0bec-4ad3-9dd5-1c7e5c0b7756, MatchProbability: 84.61%, FrameTimestamp: 10/16/2019 4:32:44 PM\n', 12, 32),
(13, '2019-10-16 16:32:47,642 [Thread:  26] INFO  [EDGE Detection] Identified Person >> Name: Chino Lyn Py Edge, PersonId: 6ce62b13-af64-4d43-9c01-2ab530ab30d7, GroupId: 684656de-0bec-4ad3-9dd5-1c7e5c0b7756, MatchProbability: 83.61%, FrameTimestamp: 10/16/2019 4:32:47 PM\n', 13, 32),
(14, '2019-10-16 16:32:50,489 [Thread:  31] INFO  [EDGE Detection] Identified Person >> Name: Chino Lyn Py Edge, PersonId: 6ce62b13-af64-4d43-9c01-2ab530ab30d7, GroupId: 684656de-0bec-4ad3-9dd5-1c7e5c0b7756, MatchProbability: 82.86%, FrameTimestamp: 10/16/2019 4:32:50 PM\n', 14, 32),
(15, '2019-10-16 16:32:53,526 [Thread:  31] INFO  [EDGE Detection] Identified Person >> Name: Chino Lyn Py Edge, PersonId: 6ce62b13-af64-4d43-9c01-2ab530ab30d7, GroupId: 684656de-0bec-4ad3-9dd5-1c7e5c0b7756, MatchProbability: 83.97%, FrameTimestamp: 10/16/2019 4:32:53 PM\n', 15, 32),
(16, '2019-10-16 16:32:56,582 [Thread:  31] INFO  [EDGE Detection] Identified Person >> Name: Chino Lyn Py Edge, PersonId: 6ce62b13-af64-4d43-9c01-2ab530ab30d7, GroupId: 684656de-0bec-4ad3-9dd5-1c7e5c0b7756, MatchProbability: 84.12%, FrameTimestamp: 10/16/2019 4:32:56 PM\n', 16, 32),
(17, '2019-10-16 16:32:59,639 [Thread:  31] INFO  [EDGE Detection] Identified Person >> Name: Chino Lyn Py Edge, PersonId: 6ce62b13-af64-4d43-9c01-2ab530ab30d7, GroupId: 684656de-0bec-4ad3-9dd5-1c7e5c0b7756, MatchProbability: 83.37%, FrameTimestamp: 10/16/2019 4:32:59 PM\n', 17, 32),
(18, '2019-10-16 16:33:02,491 [Thread:  26] INFO  [EDGE Detection] Identified Person >> Name: Chino Lyn Py Edge, PersonId: 6ce62b13-af64-4d43-9c01-2ab530ab30d7, GroupId: 684656de-0bec-4ad3-9dd5-1c7e5c0b7756, MatchProbability: 85.11%, FrameTimestamp: 10/16/2019 4:33:02 PM\n', 18, 32),
(19, '2019-10-16 16:33:05,561 [Thread:  26] INFO  [EDGE Detection] Identified Person >> Name: Chino Lyn Py Edge, PersonId: 6ce62b13-af64-4d43-9c01-2ab530ab30d7, GroupId: 684656de-0bec-4ad3-9dd5-1c7e5c0b7756, MatchProbability: 81.9%, FrameTimestamp: 10/16/2019 4:33:05 PM\n', 19, 32),
(20, '2019-10-16 16:33:08,600 [Thread:  26] INFO  [EDGE Detection] Identified Person >> Name: Chino Lyn Py Edge, PersonId: 6ce62b13-af64-4d43-9c01-2ab530ab30d7, GroupId: 684656de-0bec-4ad3-9dd5-1c7e5c0b7756, MatchProbability: 84.14%, FrameTimestamp: 10/16/2019 4:33:08 PM\n', 20, 32),
(21, '2019-10-16 16:33:11,651 [Thread:  26] INFO  [EDGE Detection] Identified Person >> Name: Chino Lyn Py Edge, PersonId: 6ce62b13-af64-4d43-9c01-2ab530ab30d7, GroupId: 684656de-0bec-4ad3-9dd5-1c7e5c0b7756, MatchProbability: 84.92%, FrameTimestamp: 10/16/2019 4:33:11 PM\n', 21, 32),
(22, '2019-10-16 16:33:14,517 [Thread:  31] INFO  [EDGE Detection] Identified Person >> Name: Chino Lyn Py Edge, PersonId: 6ce62b13-af64-4d43-9c01-2ab530ab30d7, GroupId: 684656de-0bec-4ad3-9dd5-1c7e5c0b7756, MatchProbability: 80.72%, FrameTimestamp: 10/16/2019 4:33:14 PM\n', 22, 32),
(23, '2019-10-16 16:33:17,650 [Thread:   5] INFO  [EDGE Detection] Identified Person >> Name: Chino Lyn Py Edge, PersonId: 6ce62b13-af64-4d43-9c01-2ab530ab30d7, GroupId: 684656de-0bec-4ad3-9dd5-1c7e5c0b7756, MatchProbability: 83.07%, FrameTimestamp: 10/16/2019 4:33:17 PM\n', 23, 32),
(24, '2019-10-16 16:33:20,523 [Thread:   5] INFO  [EDGE Detection] Identified Person >> Name: Chino Lyn Py Edge, PersonId: 6ce62b13-af64-4d43-9c01-2ab530ab30d7, GroupId: 684656de-0bec-4ad3-9dd5-1c7e5c0b7756, MatchProbability: 83.58%, FrameTimestamp: 10/16/2019 4:33:20 PM\n', 24, 32),
(25, '2019-10-16 16:33:23,534 [Thread:   5] INFO  [EDGE Detection] Identified Person >> Name: Chino Lyn Py Edge, PersonId: 6ce62b13-af64-4d43-9c01-2ab530ab30d7, GroupId: 684656de-0bec-4ad3-9dd5-1c7e5c0b7756, MatchProbability: 84.17%, FrameTimestamp: 10/16/2019 4:33:23 PM\n', 25, 32),
(26, '2019-10-16 16:33:26,598 [Thread:   5] INFO  [EDGE Detection] Identified Person >> Name: Chino Lyn Py Edge, PersonId: 6ce62b13-af64-4d43-9c01-2ab530ab30d7, GroupId: 684656de-0bec-4ad3-9dd5-1c7e5c0b7756, MatchProbability: 83.23%, FrameTimestamp: 10/16/2019 4:33:26 PM\n', 26, 32),
(27, '2019-10-16 16:33:29,470 [Thread:  29] INFO  [EDGE Detection] Identified Person >> Name: Chino Lyn Py Edge, PersonId: 6ce62b13-af64-4d43-9c01-2ab530ab30d7, GroupId: 684656de-0bec-4ad3-9dd5-1c7e5c0b7756, MatchProbability: 85.19%, FrameTimestamp: 10/16/2019 4:33:29 PM\n', 27, 32),
(28, '2019-10-16 16:33:32,551 [Thread:  29] INFO  [EDGE Detection] Identified Person >> Name: Chino Lyn Py Edge, PersonId: 6ce62b13-af64-4d43-9c01-2ab530ab30d7, GroupId: 684656de-0bec-4ad3-9dd5-1c7e5c0b7756, MatchProbability: 82.88%, FrameTimestamp: 10/16/2019 4:33:32 PM\n', 28, 32),
(29, '2019-10-16 16:33:35,606 [Thread:  29] INFO  [EDGE Detection] Identified Person >> Name: Chino Lyn Py Edge, PersonId: 6ce62b13-af64-4d43-9c01-2ab530ab30d7, GroupId: 684656de-0bec-4ad3-9dd5-1c7e5c0b7756, MatchProbability: 83.39%, FrameTimestamp: 10/16/2019 4:33:35 PM\n', 29, 32),
(30, '2019-10-16 16:33:38,477 [Thread:  16] INFO  [EDGE Detection] Identified Person >> Name: Chino Lyn Py Edge, PersonId: 6ce62b13-af64-4d43-9c01-2ab530ab30d7, GroupId: 684656de-0bec-4ad3-9dd5-1c7e5c0b7756, MatchProbability: 83.66%, FrameTimestamp: 10/16/2019 4:33:38 PM\n', 30, 32),
(31, '2019-10-16 16:33:41,549 [Thread:  16] INFO  [EDGE Detection] Identified Person >> Name: Chino Lyn Py Edge, PersonId: 6ce62b13-af64-4d43-9c01-2ab530ab30d7, GroupId: 684656de-0bec-4ad3-9dd5-1c7e5c0b7756, MatchProbability: 83%, FrameTimestamp: 10/16/2019 4:33:41 PM\n', 31, 32),
(32, '2019-10-16 16:33:44,655 [Thread:  16] INFO  [EDGE Detection] Identified Person >> Name: Chino Lyn Py Edge, PersonId: 6ce62b13-af64-4d43-9c01-2ab530ab30d7, GroupId: 684656de-0bec-4ad3-9dd5-1c7e5c0b7756, MatchProbability: 81.09%, FrameTimestamp: 10/16/2019 4:33:44 PM\n', 32, 32),
(33, '2019-10-16 16:33:47,477 [Thread:  26] INFO  [EDGE Detection] Identified Person >> Name: Chino Lyn Py Edge, PersonId: 6ce62b13-af64-4d43-9c01-2ab530ab30d7, GroupId: 684656de-0bec-4ad3-9dd5-1c7e5c0b7756, MatchProbability: 81.15%, FrameTimestamp: 10/16/2019 4:33:47 PM\n', 33, 32),
(34, '2019-10-16 16:33:50,883 [Thread:  26] INFO  [EDGE Detection] Identified Person >> Name: Chino Lyn Py Edge, PersonId: 6ce62b13-af64-4d43-9c01-2ab530ab30d7, GroupId: 684656de-0bec-4ad3-9dd5-1c7e5c0b7756, MatchProbability: 84.24%, FrameTimestamp: 10/16/2019 4:33:50 PM\n', 34, 32),
(35, '2019-10-16 16:33:53,637 [Thread:  26] INFO  [EDGE Detection] Identified Person >> Name: Chino Lyn Py Edge, PersonId: 6ce62b13-af64-4d43-9c01-2ab530ab30d7, GroupId: 684656de-0bec-4ad3-9dd5-1c7e5c0b7756, MatchProbability: 83.56%, FrameTimestamp: 10/16/2019 4:33:53 PM\n', 35, 32),
(36, '2019-10-16 16:33:56,474 [Thread:  16] INFO  [EDGE Detection] Identified Person >> Name: Chino Lyn Py Edge, PersonId: 6ce62b13-af64-4d43-9c01-2ab530ab30d7, GroupId: 684656de-0bec-4ad3-9dd5-1c7e5c0b7756, MatchProbability: 83.12%, FrameTimestamp: 10/16/2019 4:33:56 PM\n', 36, 32),
(37, '2019-10-16 16:33:59,535 [Thread:  16] INFO  [EDGE Detection] Identified Person >> Name: Chino Lyn Py Edge, PersonId: 6ce62b13-af64-4d43-9c01-2ab530ab30d7, GroupId: 684656de-0bec-4ad3-9dd5-1c7e5c0b7756, MatchProbability: 82.69%, FrameTimestamp: 10/16/2019 4:33:59 PM\n', 37, 32),
(38, '2019-10-16 16:34:02,583 [Thread:  16] INFO  [EDGE Detection] Identified Person >> Name: Chino Lyn Py Edge, PersonId: 6ce62b13-af64-4d43-9c01-2ab530ab30d7, GroupId: 684656de-0bec-4ad3-9dd5-1c7e5c0b7756, MatchProbability: 83.19%, FrameTimestamp: 10/16/2019 4:34:02 PM\n', 38, 32),
(39, '2019-10-16 16:34:05,647 [Thread:   5] INFO  [EDGE Detection] Identified Person >> Name: Chino Lyn Py Edge, PersonId: 6ce62b13-af64-4d43-9c01-2ab530ab30d7, GroupId: 684656de-0bec-4ad3-9dd5-1c7e5c0b7756, MatchProbability: 84.87%, FrameTimestamp: 10/16/2019 4:34:05 PM\n', 39, 32),
(40, '2019-10-16 16:34:08,512 [Thread:  29] INFO  [EDGE Detection] Identified Person >> Name: Chino Lyn Py Edge, PersonId: 6ce62b13-af64-4d43-9c01-2ab530ab30d7, GroupId: 684656de-0bec-4ad3-9dd5-1c7e5c0b7756, MatchProbability: 83.07%, FrameTimestamp: 10/16/2019 4:34:08 PM\n', 40, 32),
(41, '2019-10-16 16:34:11,642 [Thread:  29] INFO  [EDGE Detection] Identified Person >> Name: Chino Lyn Py Edge, PersonId: 6ce62b13-af64-4d43-9c01-2ab530ab30d7, GroupId: 684656de-0bec-4ad3-9dd5-1c7e5c0b7756, MatchProbability: 81.59%, FrameTimestamp: 10/16/2019 4:34:11 PM\n', 41, 32),
(42, '2019-10-16 16:34:14,704 [Thread:   5] INFO  [EDGE Detection] Identified Person >> Name: Chino Lyn Py Edge, PersonId: 6ce62b13-af64-4d43-9c01-2ab530ab30d7, GroupId: 684656de-0bec-4ad3-9dd5-1c7e5c0b7756, MatchProbability: 83.79%, FrameTimestamp: 10/16/2019 4:34:14 PM\n', 42, 32),
(43, '2019-10-16 16:34:17,547 [Thread:  29] INFO  [EDGE Detection] Identified Person >> Name: Chino Lyn Py Edge, PersonId: 6ce62b13-af64-4d43-9c01-2ab530ab30d7, GroupId: 684656de-0bec-4ad3-9dd5-1c7e5c0b7756, MatchProbability: 82.54%, FrameTimestamp: 10/16/2019 4:34:17 PM\n', 43, 32);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8_bin NOT NULL,
  `password` varchar(255) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `record`
--
ALTER TABLE `record`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `record`
--
ALTER TABLE `record`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
