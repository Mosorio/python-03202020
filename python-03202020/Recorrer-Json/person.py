, import json
import os, sys
from io import open
import datetime 
import time
import shutil
from shutil import rmtree
from os import makedirs
from os import remove
import pyautogui
import uuid 
import pymysql.cursors
from datetime import date
from datetime import datetime

#///////////////////////////////////////////
# GET-Version-Client
# Funcion para obtener la Version del Cliente, desde el archivo de Log
#//////////////////////////////////////////
def GetVersionClient():
    #///////////////////////////////////////////
    # GET-Today para seleccionar el log del cliente   
    today = date.today()
    logToday = today.strftime("%Y-%m-%d")
    log  = 'UI-' + logToday + '.log'
    #print (log)
    #input ()

    #//////////////////////////////////
	# Verificar la cantidad de archivos que existen dentro del directorio
	# Indicamos la ruta de donde se quieren leer los archivos
    path= "C:/ProgramData/Vsblty/KingSalmon/" + log
    print ("Log File: ", path)

    # La aplicaicon lee y guarada en la variable "archivo_texto" toda la informacion 
    archivo_texto = open (path, "r")

    # Se lee Linea por linea
    lineas_texto=archivo_texto.readlines()

    for ClientLine in lineas_texto:
            
        if "AppVersion" in ClientLine: #busqueda por el parametro "TextLogSearch"
            
            Pocision = ClientLine.find("AppVersion") + 10
            limite = Pocision + 13
            AppVersion = ClientLine[Pocision:limite]
            #print ("Version:", AppVersion)

    return AppVersion
# Get and Assign Version
GetVersion = GetVersionClient()


#///////////////////////////////////////////
# GET-Endpoint-Test
# Funcion para obtener de la DB, la ultima prueba que se ejecuto para el Endpoint
# Si la respuesta es Null, se TestNumero = 1
#//////////////////////////////////////////
# Connect to the database
def TestNumero():
    
    connection = pymysql.connect(host='192.168.2.161',
        user='Qatest',
        password='Quito.2019',
        db='automatizacion',
        charset='utf8mb4',
        cursorclass=pymysql.cursors.DictCursor)

    try:
        with connection.cursor() as cursor:
            # Read a single record
            sql = "SELECT `Numero_Test` FROM `json_metrics` ORDER BY `Numero_Test` DESC LIMIT 1"
            cursor.execute(sql, ())
            result = cursor.fetchone()
            #print (result)
            if result == None:
                TestNumero = 1
                #print (TestNumero)
            else:
                TestNumero = int(result.get('Numero_Test')) + 1
                #print (TestNumero)
            #input ()
    finally:
        connection.close()
    return TestNumero

Test_N = TestNumero()
print (Test_N)


#///////////////////////////////////////////
# GetFileJson
# Function to get all the files inside the usage folder
#//////////////////////////////////////////
def GetFileJson():
    #//////////////////////////////////
	# Verificar la cantidad de archivos que existen dentro del directorio
	# Indicamos la ruta de donde se quieren leer los archivos
    #path= "C:/ProgramData/Vsblty/Kiosk Framework/Usage/"
    print ("//////////////////////////////////////////////////////////////////////////")
    path= "C:/ProgramData/Vsblty/Kiosk Framework/Usage/"
    #print ("Original location of the files to process")
    #print (path)
    #print ("press enter to continue")
    #input ()

    dirs = os.listdir(path)
    i=0
    for file in dirs:
        archivo = file
        rutacompleta = path + archivo
        FileType = rutacompleta.find("json")
        if  FileType > 1:
            #while True:
            try:
                i += 1
                #DateTest = datetime.datetime.now()
                today = date.today()
                logToday = today.strftime("%Y-%m-%d")
                #print ("////////////////////////////////////////////////////////////////////////////////////////")
                #print ("/////////////////////////////////////////////************************************////////////////////////////////////////////")
                print ("**********************************************")
                
                print ("Item:", i)
                print ("File: ",archivo)
                ExcluirFIle = archivo.find("APICallsCount")
                if ExcluirFIle == 1:
                    print ("El Archivo APICallsCount no se procesara")    
                else:             
                    #print ("////////////////////////////////////////////////////////////////////////////////////////")
                    #print ("////////////////////////////////////////////////////////////////////////////////////////")
                    
                    #///////////////////////////////////////////
                    with open(rutacompleta, ) as contenido:
                        datajson = json.load(contenido)

                        #encoded
                        data_string = json.dumps(datajson)

                        #Decoded
                        decoded = json.loads(data_string)

                        data_string = json.dumps(datajson)

                        # of each json file, we get the Type engagement property
                        # ///////////////////////////////////
                        # Validacion 1 "Asset Duration"
                        # ///////////////////////////////////
                        try:
                            Assetduration = decoded["duration"]
                        except KeyError as error:
                                Assetduration = 0
                        if Assetduration < 21 and Assetduration > 1:
                                StatusAssetduration = "Pass"
                        else:
                            StatusAssetduration = "Failed"
                        
                        # ///////////////////////////////////
                        # Get data     "personEngagements"
                        # ///////////////////////////////////
                        try:
                            engagementType = decoded["personEngagements"]
                        except KeyError as error:
                            engagementType = 1

                        # verificamos cuantos Face Fueron detectados el el archivo json    
                        contador = len (engagementType) -1
                        
                        # ///////////////////////////////////
                        # Por cada FaceId detectado se realiza validaciones
                        # Bloque mas Importante, Informacion del Test
                        # ///////////////////////////////////
                        while 0 <= contador:
                            # ///////////////////////////////////
                            # Datos Demograficos
                            # ///////////////////////////////////
                            TotalEmtions = 0 #inicializando variable suamrizada de emociones
                            try:
                                Face = engagementType [contador]["faceId"]
                            except KeyError as error:
                                Face = None
                            try:
                                localPersistedFaceId = engagementType [contador]["localPersistedFaceId"]
                            except KeyError as error:
                                localPersistedFaceId = None
                            try:
                                durationPerson = engagementType [contador]["durationPerson"]
                            except KeyError as error:
                                durationPerson = 0
                            try:
                                dwellTime = engagementType [contador]["dwellTime"]
                            except KeyError as error:
                                dwellTime = 0
                            try:
                                engagedPct = engagementType [contador]["engagedPct"]
                            except KeyError as error:
                                engagedPct = 0
                            # ///////////////////////////////////
                            # Emociones
                            # ///////////////////////////////////
                            # Anger
                            try:
                                EmotionAnger = engagementType [contador]["angerPct"]
                            except KeyError as error:
                                EmotionAnger = 0
                            # Happiness
                            try:
                                EmotionHappy = engagementType [contador]["happyPct"]
                            except KeyError as error:
                                EmotionHappy = 0
                            # Neutral
                            try:
                                EmotionNeutral = engagementType [contador]["neutralPct"]
                            except KeyError as error:
                                EmotionNeutral = 0
                            # Sadness
                            try:
                                EmotionSadness = engagementType [contador]["sadnessPct"]
                            except KeyError as error:
                                EmotionSadness = 0
                            # Surprise
                            try:
                                EmotionSurprise = engagementType [contador]["surprisePct"]
                            except KeyError as error:
                                EmotionSurprise = 0
                            # Contempt
                            try:
                                EmotionContempt = engagementType [contador]["contemptPct"]
                            except KeyError as error:
                                EmotionContempt = 0
                            # Disgust
                            try:
                                EmotionDisgust = engagementType [contador]["disgustPct"]
                            except KeyError as error:
                                EmotionDisgust = 0
                            #Fear
                            try:
                                EmotionFear = engagementType [contador]["fearPct"]
                            except KeyError as error:
                                EmotionFear = 0
                            #Suma de las emociones
                            TotalEmtions = EmotionAnger + EmotionHappy + EmotionNeutral + EmotionSadness + EmotionSurprise  + EmotionContempt + EmotionDisgust + EmotionFear
                            
                            # ///////////////////////////////////
                            # Datos demograficos
                            # ///////////////////////////////////
                            demographics = engagementType [contador]["demographics"]
                            bioRecordId = demographics ["bioRecordId"]
                            age = demographics ["age"]
                            sex = demographics ["sex"]
                            # ///////////////////////////////////
                            # Clasificacion de Genero
                            # ///////////////////////////////////
                            if sex == 1:
                                Genero = "Masculino"
                            else:
                                Genero = "Femenino"
                            # ///////////////////////////////////
                            # Validacion de Name y Biorecord
                            # ///////////////////////////////////
                            if bioRecordId != "00000000-0000-0000-0000-000000000000":
                                name = demographics ["IdentityName"]
                            else:
                                name = "Unidentified Person"

                            print ("**********************************************")
                            print ("Date: ", logToday)
                            print ("Version: ", GetVersion)
                            print ("Test Nuemro: ", Test_N)
                            print ("Processed-File:", i)
                            print ("Asset Duration: ", Assetduration)
                            print ("FaceId: ", Face)
                            print ("LocalPersistedFaceId: ", localPersistedFaceId)
                            print ("**********************************************")
                            print ("DurationPerson: ", durationPerson)
                            # validacion Duration-Person contra Asset duration
                            if durationPerson <= Assetduration:
                                StatusdurationPerson = "Pass"
                            else:
                                StatusdurationPerson = "Failed"
                            # Dewelltime
                            print ("DwellTime: ", dwellTime)
                            print ("Pct-Engaged: ", engagedPct)
                            # validacion de % Dwelltime
                            try:
                                totalptc = ((dwellTime * 100) / durationPerson)
                            except ZeroDivisionError:
                                totalptc = 0
                            print ("TOTAL-DWELLTIME: ", totalptc) 
                            # validacion % Dwelltime
                            if totalptc == engagedPct and totalptc != 0:
                                StatusengagedPct = "Pass"
                            else:
                                StatusengagedPct = "Failed"
                            # Emociones
                            print ("**********************************************")
                            print ("Anger: ", EmotionAnger)
                            print ("Happiness: ", EmotionHappy)
                            print ("Neutral: ", EmotionNeutral)
                            print ("Sadness: ", EmotionSadness)
                            print ("Surprise: ", EmotionSurprise)
                            print ("Contempt: ", EmotionContempt)
                            print ("Disgust: ", EmotionDisgust)
                            print ("Fear: ", EmotionFear)
                            print ("Sum of Emotions: ", TotalEmtions)
                            # validacion Suma de Emociones
                            if TotalEmtions == 99.99 or TotalEmtions == 100 :
                                StatusTotalEmtions = "Pass"
                            else:
                                StatusTotalEmtions = "Failed"
                            # Informacion demografica
                            print ("**********************************************")
                            print ("Age: ", age)
                            print ("Genero: ", Genero)
                            print ("BioRecordId: ", bioRecordId)
                            print ("Name: ", name)
                            print ("**********************************************")
                            # Validaciones Status
                            print ("Validacion 1 (Asset Duration) is < 21 Seconds: ", StatusAssetduration)
                            print ("Validacion 2 (Person Duration) is <= (Asset Duration): ", StatusdurationPerson)
                            print ("Validacion 3 (Pct-Engaged) Dwelltime VS (Person Duration): ", StatusengagedPct)
                            print ("Validacion 4 (Sum of Emotions) is = al 100%: ", StatusTotalEmtions)
                            # Validacion structura del objeto Json, "Person-Engagements"
                            if Face == None or localPersistedFaceId == None or durationPerson == 0 or dwellTime == 0 or engagedPct == 0 or demographics == None:
                                StatusPersonEngagements = "Failed"
                            else:
                                StatusPersonEngagements = "Pass"
                            print ("Validacion 5 (Person-Engagements) Structure Json: ", StatusPersonEngagements)
                            print ("**********************************************")
                            print ("////////////////////////////////////////////////////////////////////////////////////////")
                            
                            # Validacion status
                            if StatusAssetduration == "Pass" and StatusdurationPerson == "Pass" and StatusengagedPct == "Pass" and StatusTotalEmtions == "Pass":
                            #if StatusAssetduration == "Pass":
                                StatusTest = 1
                            else:
                                StatusTest = 0
                            contador -= 1
                            #print ("---------", StatusTest)
                            # ///////////////////////////////////
                            # Connect to the database
                            # ///////////////////////////////////
                            connection = pymysql.connect(host='192.168.2.161',
                                user='Qatest',
                                password='Quito.2019',
                                db='automatizacion',
                                charset='utf8mb4',
                                cursorclass=pymysql.cursors.DictCursor)

                            try:
                                with connection.cursor() as cursor:
                            # Create a new record
                                                    
                                    sql = "INSERT INTO `json_metrics` (`Date`, `Numero_Test`, `Processed_File`, `File_Name`, `Asset_Duration`,  `FaceId`, `LocalPersistedFaceId`, `Duration_Person`, `DwellTime`, `Pct_Engaged`, `Total_DwellTime`, `E_Anger`, `E_Happiness`, `E_Neutral`,  `E_Sadness`, `E_Surprise`, `E_Contempt`, `E_Disgust`, `E_Fear`, `E_Total`, `Age`, `Id_Genero`, `Genero`,  `BioRecordId`, `Name_Person`, `Validacion1_AssetDuration`, `Validacion2_PersonDuration`, `Validacion3_DwellTime`, `Validacion4_Sum_Emotion`, `Validacion5_JsonEstructure`, `VersionClient`, `Status`) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
                                    cursor.execute(sql, (logToday, 
                                                        Test_N, 
                                                        i, 
                                                        archivo, 
                                                        Assetduration, 
                                                        Face, 
                                                        localPersistedFaceId, 
                                                        durationPerson, 
                                                        dwellTime,
                                                        engagedPct,
                                                        totalptc,
                                                        EmotionAnger,
                                                        EmotionHappy,
                                                        EmotionNeutral,
                                                        EmotionSadness,
                                                        EmotionSurprise,
                                                        EmotionContempt,
                                                        EmotionDisgust,
                                                        EmotionFear,
                                                        TotalEmtions,
                                                        age,
                                                        sex,
                                                        Genero,
                                                        bioRecordId,
                                                        name,
                                                        StatusAssetduration,
                                                        StatusdurationPerson,
                                                        StatusengagedPct,
                                                        StatusTotalEmtions,
                                                        StatusPersonEngagements,
                                                        GetVersion,
                                                        StatusTest
                                                        ))
                                    
                            # connection is not autocommit by default. So you must commit to save
                            # your changes.
                                    connection.commit()

                            finally:
                                connection.close()

                            time.sleep(0.100) 
                        
                        
                        input ()
                        #///////////////////////
                        print ("////////////////////////////////////////////////////////////////////////////////////////")
                        #print ("////////////////////////////////////////////////////////////////////////////////////////")
                        

            except UnboundLocalError:
                print ("ERROR, empty file") 
                #shutil.copy(rutacompleta, "C:/Json-Backup/EmptyFile")
                #print ("file moved to EmptyFile")
                #input ()
                pass
            except json.decoder.JSONDecodeError:
                print ("ERROR, JSON-Decode-Error") 
                #shutil.copy(rutacompleta, "C:/Json-Backup/EmptyFile")
                #input ()
                pass

            except KeyError as error:
                print ("Error, json file does not contain Name property")
                pass
            except TypeError: 
                pass

        else: 
            print("File is not of json type")


GetFileJson()

print ("----------------------------------------")

print ("------ End process------------------")
input()
#input()

