import socket
import pymysql
from flask import jsonify
from flask import Flask, current_app
import json

app = Flask(__name__)

def getdataJugador():
    with app.app_context():
        # Connect to the database
        connection = pymysql.connect(host='192.168.2.161',
                                    user='Qatest',
                                    password='Quito.2019',
                                    db='automatizacion',
                                    charset='utf8mb4',
                                    cursorclass=pymysql.cursors.DictCursor)

        try:
            with connection.cursor() as cursor:
                # Read a single record
                sql = "SELECT COUNT(Genero) AS Total FROM `json_metrics` "
                cursor.execute(sql)
                result = cursor.fetchall()
                print(result)
                #///////////////////////////////
                sql2 = "SELECT COUNT(Genero) AS Male FROM `json_metrics` WHERE `Id_Genero`=%s"
                cursor.execute(sql2, ('1'))
                resultMale = cursor.fetchall()
                print(resultMale)
                #///////////////////////////////
                sql3 = "SELECT COUNT(Id_Genero) AS Female FROM `json_metrics` WHERE `Id_Genero`=%s"
                cursor.execute(sql3, ('2'))
                resultFemale = cursor.fetchall()
                print(resultFemale)
            return jsonify(
                #show all data
                {"Total Personas": result, "Male": resultMale, "Female": resultFemale},
                {"Message": "Datos Totales por Genero"}
                #show one result of data
                #{"Data": result[0], "message": "datos Web Change"}
                )
        finally:
            connection.close()

S_socket = socket.socket()
S_socket.bind (('192.168.2.161',8030))
S_socket.listen (5)

while True:
    conexion, addr = S_socket.accept()
    print (f"Nueva Conexion Establecida: {addr}")
    
    peticion = conexion.recv(1024)
    print (peticion)

    DataFun = getdataJugador()

    #conexion.send(bytes("Hello, Respuesta desde el Servidor!", "utf-8"))
    conexion.send(string(json.dumps(DataFun), "utf-8"))
    conexion.close()