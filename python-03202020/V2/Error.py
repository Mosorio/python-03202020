# Libreria para generar GUID 
import uuid 
# Libreria para generar fecha y hora actual
import datetime
# Librerias para leer archivo, Json y log
import json
import os, sys
from io import open
import smtplib
import pymysql.cursors




#/////////////////////////////////////////////////////////////////////////
def getendpointinfo():
    
    
    # Bloque (3) Read Data Archivos Log
    # path= "C:/ProgramData/Vsblty/KingSalmon/"
    # ******************************************
    print ("**********************************")
    print ("\n")
    print ("**********************************")
    #-------------------------- Bloque 2 --------------
    # Open a file
    # Se Indica la ruta donde estan lo archivos a leer la Informacion o Log
    # Ejemplo C:\ProgramData\Vsblty\KingSalmon
    #path = "C:/Users/Mosorio/Desktop/Tool-Client/toolautometricaspy/V1/Carpeta/"
    path= "C:/ProgramData/Vsblty/KingSalmon/"
    dirs = os.listdir( path )

    # This would print all the files and directories

    for file in dirs:
        
        #print (file)
        #ruta = "C:/Users/Mosorio/Desktop/Tool-Client/toolautometricaspy/V1/Carpeta/"
        ruta = "C:/ProgramData/Vsblty/KingSalmon/"
        archivo = file
        rutacompleta = ruta + archivo
        print ("---Read in file: ", archivo)
        print ("Path: ", rutacompleta)
        
        # La aplicaicon lee y guarada en la variable "archivo_texto" toda la informacion 
        archivo_texto=open (rutacompleta, "r")

        # Se lee Linea por linea
        lineas_texto=archivo_texto.readlines()

        print ("\n")
        print ("**********************************************************************")
        print ("**************** Buscando Errores En: ", archivo)
        print ("**********************************************************************")
        #----------------Errores cliente
        # Buscar Errores del Cliente "Error"
        conta=0
        countError=0 
        # Por cada linea se valida si contiene el Texto "Starting App" y obtener la hora de ejecucion del Cliente
        for ErrorClient in lineas_texto:
                if "ERROR" in ErrorClient:
                # if "DONE" in line:
                    #///////////////////////////////////
                    # Buscando Tag Error en la Linea con el error, Variable "ErrorClient"
                    # Tag 1, [Notification Queue]
                    #TagError = (ErrorClient.count("[Notification Queue]",0,len(ErrorClient)))
                    #print ("El tag es: ", TagError)
                    #if TagError > 0:
                        #print ("TagError: [Notification Queue]")
                    #///////////////////////////////////
                    # Buscando Tag Error en la Linea con el error, Variable "ErrorClient"
                    # Tag 2, [KioskExhibitReader]
                    TagError = (ErrorClient.count("[KioskExhibitReader]",0,len(ErrorClient)))
                    #print ("El tag es: ", TagError)
                    if TagError > 0:
                        print ("TagError: [KioskExhibitReader]")
                    conta=conta+1
                    countError = countError + 1   
                    # Connect to the database
                    connection = pymysql.connect(host='localhost',
                                                user='qatest',
                                                password='Quito.2019',
                                                db='tooltest',
                                                charset='utf8mb4',
                                                cursorclass=pymysql.cursors.DictCursor)

                    try:
                        with connection.cursor() as cursor:
                            with open("C:/KioskServicesMedia/Exhibit Media/LayoutTemplate.json") as contenido:
                                EndpointInfo = json.load(contenido)

                                #encoded
                                data_string = json.dumps(EndpointInfo)

                                #Decoded
                                decoded = json.loads(data_string)
                                #print ("--- 1 --")
                                #print ('DATA:', repr(cursos))

                                data_string = json.dumps(EndpointInfo)
                                #print ("--- 2 --")
                                #print ('JSON:', data_string)
                            
                                # Asignamos a una variable el resultado,
                                # En el Primer elemento ["EndpointInfo"], es el padre de donde queremos buscar o seleccionar el valor o propiedad 
                                # En el segundo elemento ["CustomerId"], es la propiedad que deseamos obtener
                                # Asignacion de Valores

                                # ///////////////////////
                                CustomerId = str(decoded["EndpointInfo"]["CustomerId"])
                                StoreId = str(decoded["EndpointInfo"]["StoreId"])
                                EndpointId = str(decoded["EndpointInfo"]["EndpointId"])
                                HostName = str(decoded["EndpointInfo"]["EndpointName"])
                            
                                # ///////////////////////
                                # Generando GUID para Identificar la Prueba
                                # Todos los registros deben tener el mismo "GuidTest", Generado
                                # "GuidTest", una constante durante toda la Prueba
                                GuidTest = "a3021b0a-c389-47b6-bbc7-e46a94ac77cc" 
                                

                                # ///////////////////////
                                # Generando Fecha y hora actual de la prueba
                                # "DateTest" debe ser una constante durante toda la Prueba
                                DateTest = datetime.datetime.now()
                            # Create a new record
                            #sql = "INSERT INTO `error_log` (`ErrorInfo`, `TestNumer`, `ErrorNumero`) VALUES (%s, %s, %s)"
                            #cursor.execute(sql, (ErrorClient, 2, '32'))

                                sql = "INSERT INTO `error_log` (`GuidTest`, `DateTest`, `CustomerId`, `StoreId`, `EndpointId`, `HostName`, `ErrorNumero`, `ErrorInfo`, `ErrorFile`) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)"
                                cursor.execute(sql, (GuidTest, DateTest, CustomerId, StoreId, EndpointId, HostName, countError, ErrorClient, archivo))
                                #cursor.execute(sql, (ErrorClient, ErrorClient, ErrorClient, ErrorClient, ErrorClient, ErrorClient, ErrorClient))

                            # connection is not autocommit by default. So you must commit to save
                            # your changes.
                            connection.commit()
                            
                        #with connection.cursor() as cursor:
                            # Read a single record
                            #sql = "SELECT `id`, `password` FROM `users` WHERE `email`=%s"
                            #cursor.execute(sql, ('webmaster@python.org',))
                            #result = cursor.fetchone()
                            #print(result)
                    finally:
                        connection.close()
                    print (ErrorClient)
                    print ("**********************************")
                   
getendpointinfo()                   
        #input ()
print ("**************** Data Extracted Successfully *************************")
print ("**********************************************************************")
print("\n")

#/////////////////////////////////////////////////////////////////////////

