# En windows hay que installar
# pip install pipwin
# pipwin install pyaudio
# Version de Python 3.7.5
# https://adictec.com/texto-voz-en-python-con-win32com-client/

# modulo para convertir audio a texto
import speech_recognition as sr
# modulo para convertir texto a voz
import win32com.client
# Modulos para la captura de frame 
from cv2 import cv2
import sys, time

import cognitive_face as CF

r = sr.Recognizer()

with sr.Microphone() as source:
    print("Say Something...")
    audio = r.listen(source)

    try:
        # Ingles
        #text = r.recognize_google(audio, language='EN')
        # Español
        text = r.recognize_google(audio, language='ES')
        #print("What did you say: {}".format(text))
        Oracion = format(text)
        print (Oracion)
        confirmacion = Oracion.find("análisis") 
        print (confirmacion)
        if confirmacion == 0:
            print ("procesando")
            RespV = "Procesando Solicitud"
            speaker = win32com.client.Dispatch("SAPI.SpVoice")
            speaker.Speak(RespV)
            for i in range(1): 
                camera = 0
                path = 'C:/Users/Mijail QA/Documents/ToolAutomateCliente/toolautometricaspy/Valeria-Proyect/capture'
                # Take Frame to USB Camera
                #camera = cv2.VideoCapture(camera)
                # Take Frame to IP Camera
                camera = cv2.VideoCapture('http://181.199.66.129/BodyTracking/Person/VideoFrame/Captura/capture.jpg')
                print (camera)
                #time.sleep(5)
                return_value, image = camera.read() 
                cv2.imwrite('opencv'+str(i)+'.png', image)
                print ("Bloque 2")
                # Setting cognitive services
                SUBSCRIPTION_KEY = 'da6dd01edd3d452f83be14067ec192cf'
                BASE_URL = 'https://westus2.api.cognitive.microsoft.com/face/v1.0/'
                PERSON_GROUP_ID = '1bf73ad9-1aac-4cb4-b679-2efea394ee0c'
                CF.BaseUrl.set(BASE_URL)
                CF.Key.set(SUBSCRIPTION_KEY)

                # take Frame analisis
                #response = CF.face.detect('http://181.199.66.129/BodyTracking/Person/VideoFrame/Captura/capture.jpg')
                response = CF.face.detect('C:/Users/Mijail QA/Documents/ToolAutomateCliente/toolautometricaspy/Valeria-Proyect/test.jpg')
                #response = CF.face.detect(camera)
                face_ids = [d['faceId'] for d in response]
                print (face_ids)

                identified_faces = CF.face.identify(face_ids, PERSON_GROUP_ID)
                print (identified_faces)
 
                del(camera) 
                input ()
        else:
            print ("Error: Orden no esta dentro de mis capacidades")
            RespV = "Solicitud; No esta dentro de mis capacidades"
            speaker = win32com.client.Dispatch("SAPI.SpVoice")
            speaker.Speak(RespV)
        input ()
    except:
        print("I am sorry! I can not understand!")