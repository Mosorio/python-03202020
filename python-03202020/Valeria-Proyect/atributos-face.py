import requests
import json

# set to your own subscription key value
subscription_key = '19515edbb30146199ad9a7b16df98d79'
assert subscription_key

# replace <My Endpoint String> with the string from your endpoint URL
face_api_url = 'https://eastus.api.cognitive.microsoft.com/face/v1.0/detect'

image_url = 'http://181.199.66.129/camara/TWD/TWD-1elenco.jpg'


headers = {'Ocp-Apim-Subscription-Key': subscription_key}

params = {
    'returnFaceId': 'true',
    'returnFaceLandmarks': 'false',
    'returnFaceAttributes': 'age,gender,headPose,smile,facialHair,glasses,emotion,hair,makeup,occlusion,accessories,blur,exposure,noise',
}

response = requests.post(face_api_url, params=params,
                         headers=headers, json={"url": image_url})
print(json.dumps(response.json()))