import json
import os, sys
from io import open
import smtplib
# import pymysql.cursors
import datetime 
import time
import uuid 

#///////////////////////////////////////////
# Funcion para obtener todas las lineas de Identificacion, desde el archivo de Log
#//////////////////////////////////////////
def GetVersionClient():
    #//////////////////////////////////
	# Verificar la cantidad de archivos que existen dentro del directorio
	# Indicamos la ruta de donde se quieren leer los archivos
    path= "C:/Users/Mijail QA/Downloads/Vsblty_NATS_Subscriber_POC_NodeJS/files/"
    dirs = os.listdir(path)
    i = 1
    # ////////////////////////////////////////////////////////////
    # Por cada Archivo dentro del directorio, se realiza un ciclo for para recorrer cada linea y verificar hits  
    # ////////////////////////////////////////////////////////////
    for file in dirs:
        ruta = "C:/Users/Mijail QA/Downloads/Vsblty_NATS_Subscriber_POC_NodeJS/files/"
        
        archivo = file
        rutacompleta = ruta + archivo
        #print ("/////////////////////////////////////////////////////////////////////////////////////////////////////////////")
        print ("/////////////////////////////////////////////////////////////////////////////////////////////////////////////")
        # print ("File: ",rutacompleta)
        # print ("/////////////////////////////////////////////////////////////////////////////////////////////////////////////")
        #print ("/////////////////////////////////////////////////////////////////////////////////////////////////////////////")
        # La aplicaicon lee y guarada en la variable "archivo_texto" toda la informacion 
        archivo_texto=open (rutacompleta, "r")

        # Se lee Linea por linea
        lineas_texto=archivo_texto.readlines()

        # ////////////////////////////////////////////////////////////
        # Inicio de Busqueda
        # Por cada linea se valida si contiene el Texto que se desea encontrar 
        # Para este caso es: "[EDGE Detection] Identified Person >> Name:"
        # ////////////////////////////////////////////////////////////

        #///////////////////////////////////////////
        with open(rutacompleta, ) as contenido:
            datajson = json.load(contenido)

            #encoded
            data_string = json.dumps(datajson)

            #Decoded
            decoded = json.loads(data_string)

            data_string = json.dumps(datajson)
            FrameTimeComplete = decoded["FrameTimestamp"]
            FrameDate = FrameTimeComplete[0:10]
            FrameTime = FrameTimeComplete[11:-14]
            PersonName = decoded["PersonName"]
            PersonId = decoded["PersonId"]
            MatchConfidence = decoded["MatchConfidence"]
            # Frame = decoded["Data"]
            print ("Item:             ", i)
            print ("File:             ", archivo)
            print ("Date:             ", FrameDate)
            print ("Time:             ", FrameTime)
            print ("Name:             ", PersonName)
            print ("PersonId:         ", PersonId)
            print ("Match Confidence: ", MatchConfidence)
            # print ("Frame: ", Frame)

            i +=1

    return 

GetVersionClient()
input ()
