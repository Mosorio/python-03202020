import json
import os, sys
from io import open
import smtplib
import pymysql.cursors
import datetime 
import time
import uuid 



#///////////////////////////////////////////
# GET-Endpoint-Info
# Funcion para obtener la Informacion de Endpoint, desde el archivo de LayoutTemplate
#//////////////////////////////////////////



def GetEnpointSetting():
    EndpointSetting = []
    with open("C:/KioskServicesMedia/Exhibit Media/GlobalVars.json") as contenido:
        Globalvars = json.load(contenido)

        #encoded
        data_string = json.dumps(Globalvars)

        #Decoded
        decoded = json.loads(data_string)
        #print ("--- 1 --")
        #print ('DATA:', repr(Globalvars))

        data_string = json.dumps(Globalvars)
        #print ("--- 2 --")
        #print ('JSON:', data_string)
        
        # Asignamos a una variable el resultado,
        # En el Primer elemento ["List"], es el padre de donde queremos buscar o seleccionar el valor o propiedad 
        # En el segundo elemento [0], es la propiedad que deseamos obtener
        # el tercer elemeto ["Value"], es el valor de la propiedad
        # Asignacion de Valores
        print ("**********************************")
        print ("--- Get Endpoint Global Vars --")
        print ("--- Print From variables  --")
        ContentSwappedInterval = str(decoded["List"][10]["Value"])
        CameraHardwareName = str(decoded["List"][16]["Value"])
        IPAddress = str(decoded["List"][19]["Value"])
        IsPollingCamera =  str(decoded["List"][27]["Value"])
        PollingCameraInterval =  str(decoded["List"][28]["Value"])
        #SnapshotURL =  str(decoded["List"][32]["Value"])
        CollectAnalytics =  str(decoded["List"][46]["Value"])
        #ConfidenceThresholdVizsafe =  str(decoded["List"][47]["Value"])
        DemograhicDataExpirationTime =  str(decoded["List"][51]["Value"])
        EmailPublisherBioRecordExpiration  =  str(decoded["List"][52]["Value"])
        EmailPublisherIdentificationTriggerThreshold = str(decoded["List"][54]["Value"])
        EnableUseVizsafe = str(decoded["List"][58]["Value"])
        IsBodyDetectionTurnedOn = str(decoded["List"][63]["Value"])
        Sad = str(decoded["List"][64]["Value"])
        IsIdentitySearchTurnedOn = str(decoded["List"][66]["Value"])
        LiveEndpointData = str(decoded["List"][68]["Value"])
        NotificationEmailIdentifiacion = str(decoded["List"][69]["Value"])
        ObjectDetection = str(decoded["List"][72]["Value"])
        #NotificationEmailObjectDetection = str(decoded["List"][74]["Value"])
        FaceDetectionAggressiveness = str(decoded["List"][83]["Value"])
        OpenVinoFace = str(decoded["List"][84]["Value"])
        FaceDetectionModel = str(decoded["List"][85]["Value"])
        PersonDetectionType = str(decoded["List"][91]["Value"])
        ReocurringVisitor = str(decoded["List"][93]["Value"])
        #MaximumNumberOfPersonsInFRDatabase = str(decoded["List"][93]["Value"])
        #NotificationEmailReocurringVisitor = str(decoded["List"][94]["Value"])
        #TimeSendInfoToVizsafe = str(decoded["List"][96]["Value"])
        # Seleccionar informacnion de camera List
        #CameraUse = str(decoded["CameraList"][0]["CameraUse"])
        EndpointSetting = [ContentSwappedInterval,CameraHardwareName, IPAddress, IsPollingCamera, PollingCameraInterval, CollectAnalytics, DemograhicDataExpirationTime, EmailPublisherBioRecordExpiration, EmailPublisherIdentificationTriggerThreshold, EnableUseVizsafe, IsBodyDetectionTurnedOn, Sad, IsIdentitySearchTurnedOn, LiveEndpointData, NotificationEmailIdentifiacion, ObjectDetection, FaceDetectionAggressiveness, OpenVinoFace, FaceDetectionModel, PersonDetectionType, ReocurringVisitor]
        
        # Send Print
        
        print ("Content Swapped Interval: ",ContentSwappedInterval)
        print ("Camara: ",CameraHardwareName)
        print ("Camara: ",IPAddress)
        print ("Polling Camera: ",IsPollingCamera)
        print ("Polling Camera Interval: ",PollingCameraInterval)
        #print ("SnapshotURL: ", SnapshotURL)
        print ("Collect Analytics: ", CollectAnalytics)
        #print ("Confidence Threshold Vizsafe: ", ConfidenceThresholdVizsafe)
        print ("Demograhic Data Expiration: ", DemograhicDataExpirationTime)
        print ("Email Publisher BioRecord Expiration: ", EmailPublisherBioRecordExpiration)
        print ("Match confidence level: ", EmailPublisherIdentificationTriggerThreshold)
        print ("Vizsafe: ", EnableUseVizsafe)
        print ("Body Detection: ",IsBodyDetectionTurnedOn)
        print ("SAD: ",Sad)
        print ("Identifiacion: ",IsIdentitySearchTurnedOn)
        print ("Live Endpoint Data: ",LiveEndpointData)
        print ("Notification Email (Identifiacion): ",NotificationEmailIdentifiacion)
        print ("Object Detection: ", ObjectDetection)
        #print ("Notification Email (Object-Detection): ",NotificationEmailObjectDetection)
        print ("Face Detection Openvino: ",OpenVinoFace)
        print ("Face Detection Aggressiveness: ",FaceDetectionAggressiveness)
        print ("Face Detection Model Openvino: ",FaceDetectionModel)
        print ("Person Detection Type: ",PersonDetectionType)
        print ("Reocurring Visitor: ",ReocurringVisitor)
        #print ("Maximum Number Of Persons In FR Database: ",MaximumNumberOfPersonsInFRDatabase)
        #print ("Notification Email Reocurring Visitor: ",NotificationEmailReocurringVisitor)
        #print ("Time Send Info to Vizsafe:",TimeSendInfoToVizsafe)
        
        #camera-List
        #print ("CameraUse: ",CameraUse)
    return EndpointSetting
        
x = GetEnpointSetting()
print (x)
input ()