from rest_framework import serializers

class Userserializers(serializers.Serializer):
    id = serializers.ReadOnlyField()
    nombre = serializers.CharField()
    apellido = serializers.CharField()