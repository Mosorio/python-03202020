import json
import os, sys
from io import open
import smtplib
import pymysql.cursors

#********************************
#******** Descripcion del Archivo ********
#*****************************************
# Este archivo realiza
# Get Endpoint Info (layoutTemplate)
# Get data Global vars
# Identifica todos lo archivos log en la carperta KingSalmon
# recorre cada archivo log y busca Errores
# Evia Email Con el detalle de la Prueba
# Falta Integrar la insersion DB 

#declaracion de variables
countStarting = 0
countEnd = 0
countError = 0
conta=0
line=""


# Bloque GET Endpoint Info
# LayoutTemplate
print ("**********************************")
def cargar_datos(ruta1):
    with open(ruta1) as contenido:
        EndpointInfo = json.load(contenido)

        #encoded
        data_string = json.dumps(EndpointInfo)

        #Decoded
        decoded = json.loads(data_string)
        #print ("--- 1 --")
        #print ('DATA:', repr(cursos))

        data_string = json.dumps(EndpointInfo)
        #print ("--- 2 --")
        #print ('JSON:', data_string)
        
              
        # Asignamos a una variable el resultado,
        # En el Primer elemento ["EndpointInfo"], es el padre de donde queremos buscar o seleccionar el valor o propiedad 
        # En el segundo elemento ["CustomerId"], es la propiedad que deseamos obtener
        # Asignacion de Valores
        
        print ("--- Get Endpoint Info --")
        print ("--- Print From variables  --")
        CustomerId = str(decoded["EndpointInfo"]["CustomerId"])
        StoreId = str(decoded["EndpointInfo"]["StoreId"])
        EndpointId = str(decoded["EndpointInfo"]["EndpointId"])
        HostName = str(decoded["EndpointInfo"]["EndpointName"])
        # Send Print
        print ("CustomerId: ",CustomerId)
        print ("   StoreId: ",StoreId)
        print ("EndpointId: ",EndpointId)
        print ("  HostName: ",HostName)
        
# En este Bloque asignamos la Ruta del archivo que deseamos Obtener los datos
if __name__ == '__main__':

    #ruta = 'C:/KioskServicesMedia/Exhibit Media/LayoutTemplateC.json'
    ruta1 = 'C:/KioskServicesMedia/Exhibit Media/LayoutTemplate.json'
    
    cargar_datos(ruta1)

# Bloque GET Endpoint Global vars
# Gloval Vars
print ("**********************************")
print ("\n")
def cargar_datos(ruta):
    with open(ruta) as contenido:
        Globalvars = json.load(contenido)

        #encoded
        data_string = json.dumps(Globalvars)

        #Decoded
        decoded = json.loads(data_string)
        #print ("--- 1 --")
        #print ('DATA:', repr(Globalvars))

        data_string = json.dumps(Globalvars)
        #print ("--- 2 --")
        #print ('JSON:', data_string)
        
        # Asignamos a una variable el resultado,
        # En el Primer elemento ["List"], es el padre de donde queremos buscar o seleccionar el valor o propiedad 
        # En el segundo elemento [0], es la propiedad que deseamos obtener
        # el tercer elemeto ["Value"], es el valor de la propiedad
        # Asignacion de Valores
        print ("**********************************")
        print ("--- Get Endpoint Global Vars --")
        print ("--- Print From variables  --")
        ContentSwappedInterval = str(decoded["List"][10]["Value"])
        CameraHardwareName = str(decoded["List"][16]["Value"])
        IPAddress = str(decoded["List"][19]["Value"])
        IsPollingCamera =  str(decoded["List"][27]["Value"])
        PollingCameraInterval =  str(decoded["List"][28]["Value"])
        SnapshotURL =  str(decoded["List"][32]["Value"])
        CollectAnalytics =  str(decoded["List"][46]["Value"])
        ConfidenceThresholdVizsafe =  str(decoded["List"][47]["Value"])
        DemograhicDataExpirationTime =  str(decoded["List"][51]["Value"])
        EmailPublisherBioRecordExpiration  =  str(decoded["List"][52]["Value"])
        EmailPublisherIdentificationTriggerThreshold = str(decoded["List"][54]["Value"])
        EnableUseVizsafe = str(decoded["List"][58]["Value"])
        IsBodyDetectionTurnedOn = str(decoded["List"][63]["Value"])
        Sad = str(decoded["List"][64]["Value"])
        IsIdentitySearchTurnedOn = str(decoded["List"][66]["Value"])
        LiveEndpointData = str(decoded["List"][68]["Value"])
        NotificationEmailIdentifiacion = str(decoded["List"][69]["Value"])
        ObjectDetection = str(decoded["List"][71]["Value"])
        NotificationEmailObjectDetection = str(decoded["List"][74]["Value"])
        FaceDetectionAggressiveness = str(decoded["List"][82]["Value"])
        OpenVinoFace = str(decoded["List"][83]["Value"])
        FaceDetectionModel = str(decoded["List"][84]["Value"])
        PersonDetectionType = str(decoded["List"][90]["Value"])
        ReocurringVisitor = str(decoded["List"][92]["Value"])
        MaximumNumberOfPersonsInFRDatabase = str(decoded["List"][93]["Value"])
        NotificationEmailReocurringVisitor = str(decoded["List"][94]["Value"])
        TimeSendInfoToVizsafe = str(decoded["List"][96]["Value"])
        # Seleccionar informacnion de camera List
        #CameraUse = str(decoded["CameraList"][0]["CameraUse"])
        
        # Send Print
        print ("Content Swapped Interval: ",ContentSwappedInterval)
        print ("Camara: ",CameraHardwareName)
        print ("Camara: ",IPAddress)
        print ("Polling Camera: ",IsPollingCamera)
        print ("Polling Camera Interval: ",PollingCameraInterval)
        print ("SnapshotURL: ", SnapshotURL)
        print ("Collect Analytics: ", CollectAnalytics)
        print ("Confidence Threshold Vizsafe: ", ConfidenceThresholdVizsafe)
        print ("Demograhic Data Expiration: ", DemograhicDataExpirationTime)
        print ("Email Publisher BioRecord Expiration: ", EmailPublisherBioRecordExpiration)
        print ("Match confidence level: ", EmailPublisherIdentificationTriggerThreshold)
        print ("Vizsafe: ", EnableUseVizsafe)
        print ("Body Detection: ",IsBodyDetectionTurnedOn)
        print ("SAD: ",Sad)
        print ("Identifiacion: ",IsIdentitySearchTurnedOn)
        print ("Live Endpoint Data: ",LiveEndpointData)
        print ("Notification Email (Identifiacion): ",NotificationEmailIdentifiacion)
        print ("Object Detection: ", ObjectDetection)
        print ("Notification Email (Object-Detection): ",NotificationEmailObjectDetection)
        print ("Face Detection Openvino: ",OpenVinoFace)
        print ("Face Detection Aggressiveness: ",FaceDetectionAggressiveness)
        print ("Face Detection Model Openvino: ",FaceDetectionModel)
        print ("Person Detection Type: ",PersonDetectionType)
        print ("Reocurring Visitor: ",ReocurringVisitor)
        print ("Maximum Number Of Persons In FR Database: ",MaximumNumberOfPersonsInFRDatabase)
        print ("Notification Email Reocurring Visitor: ",NotificationEmailReocurringVisitor)
        print ("Time Send Info to Vizsafe:",TimeSendInfoToVizsafe)

        #camera-List
        #print ("CameraUse: ",CameraUse)
        
# En este Bloque asignamos la Ruta del archivo que deseamos Obtener los datos
if __name__ == '__main__':

    #ruta = 'C:/KioskServicesMedia/Exhibit Media/LayoutTemplateC.json'
    ruta = 'C:/KioskServicesMedia/Exhibit Media/GlobalVars.json'
    
    cargar_datos(ruta)

# Bloque (3) Read Data Archivos Log
# path= "C:/ProgramData/Vsblty/KingSalmon/"
# ******************************************
print ("**********************************")
print ("\n")
print ("**********************************")
#-------------------------- Bloque 2 --------------
# Open a file
# Se Indica la ruta donde estan lo archivos a leer la Informacion o Log
# Ejemplo C:\ProgramData\Vsblty\KingSalmon
#path = "C:/Users/Mosorio/Desktop/Tool-Client/toolautometricaspy/V1/Carpeta/"
path= "C:/ProgramData/Vsblty/KingSalmon/"
dirs = os.listdir( path )

# This would print all the files and directories

for file in dirs:
    
    #print (file)
    #ruta = "C:/Users/Mosorio/Desktop/Tool-Client/toolautometricaspy/V1/Carpeta/"
    ruta = "C:/ProgramData/Vsblty/KingSalmon/"
    archivo = file
    rutacompleta = ruta + archivo
    print ("---Read in file: ", archivo)
    print ("Path: ", rutacompleta)
    
    # La aplicaicon lee y guarada en la variable "archivo_texto" toda la informacion 
    archivo_texto=open (rutacompleta, "r")

    # Se lee Linea por linea
    lineas_texto=archivo_texto.readlines()

    #----------------Start cliente
    # Buscar Ejecucion del Cliente "Date start"
    # Por cada linea se valida si contiene el Texto "Starting App" y obtener la hora de ejecucion del Cliente
    for StartClient in lineas_texto:
            if "Starting App" in StartClient:
            # if "DONE" in line:
                DateRunClient=StartClient[:10]
                RunClient=StartClient[11:19]
                print (StartClient)
                countStarting = countStarting + 1
    #input ()

    #----------------End cliente
    # Buscar stop del Cliente "Stop Client"
    # Por cada linea se valida si contiene el Texto "Starting App" y obtener la hora de ejecucion del Cliente
    for EndClient in lineas_texto:
            if "Application exiting" in EndClient:
            # if "DONE" in line:
                StopClient=EndClient[11:19]
                print (EndClient)
                countEnd = countEnd + 1
    #input ()
    print ("\n")
    print ("**********************************************************************")
    print ("**************** Buscando Errores En: ", archivo)
    print ("**********************************************************************")
    #----------------Errores cliente
    # Buscar Errores del Cliente "Error"
    # Por cada linea se valida si contiene el Texto "Starting App" y obtener la hora de ejecucion del Cliente
    for ErrorClient in lineas_texto:
            if "ERROR" in ErrorClient:
            # if "DONE" in line:
                print (ErrorClient)
                conta=conta+1
                        
                # Connect to the database
                connection = pymysql.connect(host='192.168.100.155',
                                            user='qatest',
                                            password='Quito.2019',
                                            db='tooltest',
                                            charset='utf8mb4',
                                            cursorclass=pymysql.cursors.DictCursor)

                try:
                    with connection.cursor() as cursor:
                        # Create a new record
                        sql = "INSERT INTO `record` (`InfoLine`, `TotalIdentificacion`, `NumeroTest`) VALUES (%s, %s, %s)"
                        cursor.execute(sql, (ErrorClient, conta, '32'))

                    # connection is not autocommit by default. So you must commit to save
                    # your changes.
                    connection.commit()

                    with connection.cursor() as cursor:
                        # Read a single record
                        sql = "SELECT `id`, `password` FROM `users` WHERE `email`=%s"
                        cursor.execute(sql, ('webmaster@python.org',))
                        result = cursor.fetchone()
                        print(result)
                finally:
                    connection.close()
                print (ErrorClient)
                print ("**********************************")
                countError = countError + 1
                
    #input ()
print ("**************** Data Extracted Successfully *************************")
print ("**********************************************************************")
print("\n")

# Bloque (4) Resumen de la Prueba
# ********
# ******************************************
print ("*********************************************")
print ("******** Datalle del Test ************")
print ("*********************************************")
#print ("Starting App Total: ",countStarting)
print ("- Starting App Date: ",DateRunClient)
print ("- Run Client (Starting App Time): ",RunClient)
#print ("Application Exiting Total: ",countStarting)
print ("- Stop Client (Application Exiting Time): ",StopClient)
print ("*********************************************")
print ("******** Error ************")
print ("*********************************************")
print ("- Total de Errores: ",countError)
print ("*********************************************")
print ("******** Send Email With Report ************")
print ("*********************************************")

# Bloque (5) Send Email
# ********
# ******************************************
print ("*********************************************")


# Bloque GET Endpoint Info
# LayoutTemplate
print ("**********************************")
def cargar_datos(ruta1):
    with open(ruta1) as contenido:
        EndpointInfo = json.load(contenido)

        #encoded
        data_string = json.dumps(EndpointInfo)

        #Decoded
        decoded = json.loads(data_string)
        #print ("--- 1 --")
        #print ('DATA:', repr(cursos))

        data_string = json.dumps(EndpointInfo)
        #print ("--- 2 --")
        #print ('JSON:', data_string)
        
              
        # Asignamos a una variable el resultado,
        # En el Primer elemento ["EndpointInfo"], es el padre de donde queremos buscar o seleccionar el valor o propiedad 
        # En el segundo elemento ["CustomerId"], es la propiedad que deseamos obtener
        # Asignacion de Valores
        
        print ("--- Get Endpoint Info --")
        print ("--- Print From variables  --")
        CustomerId = str(decoded["EndpointInfo"]["CustomerId"])
        StoreId = str(decoded["EndpointInfo"]["StoreId"])
        EndpointId = str(decoded["EndpointInfo"]["EndpointId"])
        HostName = str(decoded["EndpointInfo"]["EndpointName"])
        # Send Print
        #print ("CustomerId: ",CustomerId)
        #print ("   StoreId: ",StoreId)
        #print ("EndpointId: ",EndpointId)
        #print ("  HostName: ",HostName)

        message2 = "Resumen, Test Automatizado \n"
        
        message = message2 + '\n Endpoint: ' + HostName + '\n EndpointId: ' + EndpointId + '\n CustomerId: ' + CustomerId + '\n StoreId: ' + StoreId + '\n Date Logs: ' + DateRunClient + '\n Run Client: ' + RunClient + '\n Stop Client: ' + StopClient  + '\n Errores: ' 
        #message = message2
        asunto = "Resumen Test automatizado Mertricas. HostName: "
        subject = asunto + HostName
        #subject = asunto

        message = "Subject: {}\n\n{}".format(subject, message)

        server = smtplib.SMTP("smtp.gmail.com", 587)
        server.starttls()
        server.login("mijail.test4@gmail.com", "58722191")
            
        #server.sendmail("mijail.test4@gmail.com", "carlos.silva@logicstudio.net", message)
        server.sendmail("mijail.test4@gmail.com", "mijail.test3@gmail.com", message)

        server.quit()

        print ("Correo enviado correctamente!")
        print ("*********************************************")
        print ("*********************************************")
        #input ()

        
# En este Bloque asignamos la Ruta del archivo que deseamos Obtener los datos
if __name__ == '__main__':

    #ruta = 'C:/KioskServicesMedia/Exhibit Media/LayoutTemplateC.json'
    ruta1 = 'C:/KioskServicesMedia/Exhibit Media/LayoutTemplate.json'
    
    cargar_datos(ruta1)




