#!/usr/bin/python

import os, sys
from io import open
import smtplib
#from decouple import config

countStarting= 0
countEnd = 0
countError = 0

print ("**************** Extracting data from the Log Files ******************")
print ("**********************************************************************")
print("\n")
#--------------------------- Bloque 1 --------------
# Asignamos ruta del archivo LayoutTemplate obtener datos de Endpoint
archivo_texto=open ("C:/KioskServicesMedia/Exhibit Media/LayoutTemplate.json", "r")

# guardamos los datos leidos de /LayoutTemplate
lineas_Layout=archivo_texto.readlines()

#-------------------
# Selecionar datos del Endpoint
# Selecionar Datos del Customer Endpoint Test
for Customer in lineas_Layout:
    if "CustomerId" in Customer:
        # recortar string primeros 23 
        CustomerId=Customer[17:53]
        #print (Customer)
        #print (CustomerId)
        #input()

#-------------------
# Selecionar Nombre del Endpoint 
for EndpointName in lineas_Layout:
    if "EndpointName" in EndpointName:
        # recortar string primeros 23 
        HostName=EndpointName[19:34]
        ##print (EndpointName)
        #print (HostName)
        #input()

#-------------------
# Seleccionar Id del endpoint
for EndpointID in lineas_Layout:
    if "EndpointId" in EndpointID:
        # recortar string primeros 23 
        GuidEndpoint=EndpointID[17:53]
        #print (EndpointID)
        #print (GuidEndpoint)
        #input()

#-------------------------- Bloque 2 --------------
# Open a file
# Se Indica la ruta donde estan lo archivos a leer la Informacion o Log
# Ejemplo C:\ProgramData\Vsblty\KingSalmon
#path = "C:/Users/Mosorio/Desktop/Tool-Client/toolautometricaspy/V1/Carpeta/"
path= "C:/ProgramData/Vsblty/KingSalmon/"
dirs = os.listdir( path )

# This would print all the files and directories

for file in dirs:
    
    #print (file)
    #ruta = "C:/Users/Mosorio/Desktop/Tool-Client/toolautometricaspy/V1/Carpeta/"
    ruta = "C:/ProgramData/Vsblty/KingSalmon/"
    archivo = file
    rutacompleta = ruta + archivo
    print ("---Read in file: ", archivo)
    print ("Path: ", rutacompleta)
    
    # La aplicaicon lee y guarada en la variable "archivo_texto" toda la informacion 
    archivo_texto=open (rutacompleta, "r")

    # Se lee Linea por linea
    lineas_texto=archivo_texto.readlines()

    #----------------Start cliente
    # Buscar Ejecucion del Cliente "Date start"
    # Por cada linea se valida si contiene el Texto "Starting App" y obtener la hora de ejecucion del Cliente
    for StartClient in lineas_texto:
            if "Starting App" in StartClient:
            # if "DONE" in line:
                DateRunClient=StartClient[:10]
                RunClient=StartClient[11:19]
                print (StartClient)
                countStarting = countStarting + 1
    #input ()

    #----------------End cliente
    # Buscar stop del Cliente "Stop Client"
    # Por cada linea se valida si contiene el Texto "Starting App" y obtener la hora de ejecucion del Cliente
    for EndClient in lineas_texto:
            if "Application exiting" in EndClient:
            # if "DONE" in line:
                StopClient=EndClient[11:19]
                print (EndClient)
                countEnd = countEnd + 1
    #input ()
    print ("**********************************************************************")
    print ("**************** Buscando Errores En: ", archivo)
    print ("**********************************************************************")
    #----------------Errores cliente
    # Buscar Errores del Cliente "Error"
    # Por cada linea se valida si contiene el Texto "Starting App" y obtener la hora de ejecucion del Cliente
    for ErrorClient in lineas_texto:
            if "ERROR" in ErrorClient:
            # if "DONE" in line:
                
                print (ErrorClient)
                countError = countError + 1
    #input ()
print ("**************** Data Extracted Successfully *************************")
print ("**********************************************************************")
print("\n")


# Resumen de la Prueba
print ("********** Resumen Test Metricas ************")
print ("*********************************************")
print ("******** Datos Endpoint **********")
print ("*********************************************")
print ("- CustomerID: ",CustomerId)
print ("- HostName: ",HostName)
print ("- EndpointId: ",GuidEndpoint)
print ("*********************************************")
print ("******** Global Vars ************")
print ("*********************************************")
print ("- Services: Cloud/Edge")
print ("- ContentSwappedInterval: 20 segundos")
print ("- EmailPublisherBioRecordExpiration: 1 Minuto")
print ("- IdentificationTriggerThreshold: 55%")
print ("- IsIdentitySearch: True")
print ("- SAD: True")
print ("- BodyDetection: True")
print ("- LiveEndpointData: True")
print ("- Objectdetection: True")
print ("- Rocurring Visitor: True")
print ("- Data Captor Mode: False")
print ("*********************************************")
print ("******** Camera Setting ************")
print ("*********************************************")
print ("- Camera Mode: Simple/Multiple/Polling")
print ("- Camera Main: IP/USB")
print ("*********************************************")
print ("******** Date Test ************")
print ("*********************************************")
#print ("Starting App Total: ",countStarting)
print ("- Starting App Date: ",DateRunClient)
print ("- Run Client (Starting App Time): ",RunClient)
#print ("Application Exiting Total: ",countStarting)
print ("- Stop Client (Application Exiting Time): ",StopClient)
print ("*********************************************")
print ("******** Error ************")
print ("*********************************************")
print ("- Total de Errores: ",countError)
print ("*********************************************")
print ("******** Send Email With Report ************")
print ("*********************************************")



message2 = "Resumen, Test Automatizado \n"
message = message2 + HostName + GuidEndpoint + DateRunClient + RunClient + StopClient 
asunto = "Resumen Test automatizado Mertricas. HostName: "
subject = asunto + HostName

message = "Subject: {}\n\n{}".format(subject, message)

server = smtplib.SMTP("smtp.gmail.com", 587)
server.starttls()
server.login("mijail.test4@gmail.com", "58722191")
    
server.sendmail("mijail.test4@gmail.com", "carlos.silva@logicstudio.net", message)

server.quit()

print ("Correo enviado correctamente!")

input ()

input ()

