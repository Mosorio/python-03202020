import json
import os, sys
from io import open
import smtplib
import pymysql.cursors
import datetime 
import time
import uuid 

#///////////////////////////////////////////
# GET-Endpoint-Setting
# Funcion para obtener la Configuracion del Endpoint, desde el archivo de Globalvars.json
#//////////////////////////////////////////
def GetEnpointSetting():
    #Iniciamos Lista que contendra toda la configuracion del Endpoint
    EndpointSetting = []
    with open("C:/KioskServicesMedia/Exhibit Media/GlobalVars.json") as contenido:
        Globalvars = json.load(contenido)

        #encoded
        data_string = json.dumps(Globalvars)

        #Decoded
        decoded = json.loads(data_string)
        #print ("--- 1 --")
        #print ('DATA:', repr(Globalvars))

        data_string = json.dumps(Globalvars)
        #print ("--- 2 --")
        #print ('JSON:', data_string)
        
        # Asignamos a una variable el resultado,
        # En el Primer elemento ["List"], es el padre de donde queremos buscar o seleccionar el valor o propiedad 
        # En el segundo elemento [0], es la propiedad que deseamos obtener
        # el tercer elemeto ["Value"], es el valor de la propiedad
        # Asignacion de Valores
        #print ("**********************************")
        #print ("--- Get Endpoint Global Vars --")
        ContentSwappedInterval = str(decoded["List"][10]["Value"])
        CameraHardwareName = str(decoded["List"][16]["Value"])
        IPAddress = str(decoded["List"][19]["Value"])
        IsPollingCamera =  str(decoded["List"][27]["Value"])
        PollingCameraInterval =  str(decoded["List"][28]["Value"])
        #SnapshotURL =  str(decoded["List"][32]["Value"])
        CollectAnalytics =  str(decoded["List"][46]["Value"])
        #ConfidenceThresholdVizsafe =  str(decoded["List"][47]["Value"])
        DemograhicDataExpirationTime =  str(decoded["List"][51]["Value"])
        EmailPublisherBioRecordExpiration  =  str(decoded["List"][52]["Value"])
        EmailPublisherIdentificationTriggerThreshold = str(decoded["List"][54]["Value"])
        EnableUseVizsafe = str(decoded["List"][58]["Value"])
        IsBodyDetectionTurnedOn = str(decoded["List"][63]["Value"])
        Sad = str(decoded["List"][64]["Value"])
        IsIdentitySearchTurnedOn = str(decoded["List"][66]["Value"])
        LiveEndpointData = str(decoded["List"][68]["Value"])
        NotificationEmailIdentifiacion = str(decoded["List"][69]["Value"])
        Nats = str(decoded["List"][71]["Value"])
        #NotificationEmailObjectDetection = str(decoded["List"][74]["Value"])
        edgeactive = str(decoded["List"][89]["Value"])
        OpenVinoFace = str(decoded["List"][83]["Value"])
        FaceDetectionModel = str(decoded["List"][84]["Value"])
        PersonDetectionType = str(decoded["List"][90]["Value"])
        ReocurringVisitor = str(decoded["List"][92]["Value"])
        #MaximumNumberOfPersonsInFRDatabase = str(decoded["List"][93]["Value"])
        #NotificationEmailReocurringVisitor = str(decoded["List"][94]["Value"])
        #TimeSendInfoToVizsafe = str(decoded["List"][96]["Value"])
        # Seleccionar informacnion de camera List
        #CameraUse = str(decoded["CameraList"][0]["CameraUse"])

        #///////////////////////
        # Asignamos configuracion a la Lista
        #///////////////////////
        EndpointSetting = [ContentSwappedInterval,CameraHardwareName, IPAddress, IsPollingCamera, PollingCameraInterval, CollectAnalytics, DemograhicDataExpirationTime, EmailPublisherBioRecordExpiration, EmailPublisherIdentificationTriggerThreshold, EnableUseVizsafe, IsBodyDetectionTurnedOn, Sad, IsIdentitySearchTurnedOn, LiveEndpointData, NotificationEmailIdentifiacion, Nats, edgeactive, OpenVinoFace, FaceDetectionModel, PersonDetectionType, ReocurringVisitor]
      
    return EndpointSetting
#print (GetEnpointSetting())
EndpointSetting = GetEnpointSetting()
Nats = EndpointSetting[14]
edge = EndpointSetting[16]

print (Nats)
print (edge)
#EndpointSetting = GetEnpointSetting()