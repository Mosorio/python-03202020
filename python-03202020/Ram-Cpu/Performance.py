import psutil
import pyautogui
import uuid 
import os
import pymysql.cursors
import datetime 
import time
import json
import os, sys
from io import open
from shutil import rmtree 
from datetime import date
from datetime import datetime


#///////////////////////////////////////////
# GET-Version-Client
# Funcion para obtener la Version del Cliente, desde el archivo de Log
#//////////////////////////////////////////
def GetVersionClient():
    #///////////////////////////////////////////
    # GET-Today para seleccionar el log del cliente   
    today = date.today()
    logToday = today.strftime("%Y-%m-%d")
    log  = 'UI-' + logToday + '.log'
    #print (log)
    #input ()

    #//////////////////////////////////
	# Verificar la cantidad de archivos que existen dentro del directorio
	# Indicamos la ruta de donde se quieren leer los archivos
    path= "C:/ProgramData/Vsblty/KingSalmon/" + log
    print ("Log File: ", path)

    # La aplicaicon lee y guarada en la variable "archivo_texto" toda la informacion 
    archivo_texto = open (path, "r")

    # Se lee Linea por linea
    lineas_texto=archivo_texto.readlines()

    for ClientLine in lineas_texto:
            
        if "AppVersion" in ClientLine: #busqueda por el parametro "TextLogSearch"
            
            Pocision = ClientLine.find("AppVersion") + 10
            limite = Pocision + 13
            AppVersion = ClientLine[Pocision:limite]
            #print ("Version:", AppVersion)

    return AppVersion



#///////////////////////////////////////////
# GET-Endpoint-Setting
# Funcion para obtener la Configuracion del Endpoint, desde el archivo de Globalvars.json
#//////////////////////////////////////////
def GetEnpointSetting():
    #Iniciamos Lista que contendra toda la configuracion del Endpoint
    EndpointSetting = []
    with open("C:/KioskServicesMedia/Exhibit Media/GlobalVars.json") as contenido:
        Globalvars = json.load(contenido)

        #encoded
        data_string = json.dumps(Globalvars)

        #Decoded
        decoded = json.loads(data_string)
        #print ("--- 1 --")
        #print ('DATA:', repr(Globalvars))

        data_string = json.dumps(Globalvars)
        #print ("--- 2 --")
        #print ('JSON:', data_string)
        
        # Asignamos a una variable el resultado,
        # En el Primer elemento ["List"], es el padre de donde queremos buscar o seleccionar el valor o propiedad 
        # En el segundo elemento [0], es la propiedad que deseamos obtener
        # el tercer elemeto ["Value"], es el valor de la propiedad
        # Asignacion de Valores
        
        Nats = str(decoded["List"][70]["Value"])
        OpenVinoFace = str(decoded["List"][90]["Value"])
        
        #///////////////////////
        # Asignamos configuracion a la Lista
        #///////////////////////
        EndpointSetting = [Nats, OpenVinoFace]
        #print ("--------------------------------------------")
        #print (EndpointSetting)
        #print ("--------------------------------------------")
        #print ("Nats: ", Nats)
        #print ("Edge: ", OpenVinoFace)
        
    return EndpointSetting

def getinfo():
    with open("C:/KioskServicesMedia/Exhibit Media/LayoutTemplate.json") as contenido:
        EndpointInfo = json.load(contenido)

        #encoded
        data_string = json.dumps(EndpointInfo)

        #Decoded
        decoded = json.loads(data_string)

        data_string = json.dumps(EndpointInfo)      
       
        # Asignamos a una variable el resultado,
        CustomerId = str(decoded["EndpointInfo"]["CustomerId"])
        StoreId = str(decoded["EndpointInfo"]["StoreId"])
        EndpointId = str(decoded["EndpointInfo"]["EndpointId"])
        HostName = str(decoded["EndpointInfo"]["EndpointName"])

        #///////////////////////
        # Asignamos configuracion a la Lista
        #///////////////////////
        GetInfoENdpoint  = [CustomerId, StoreId,EndpointId, HostName]
        #print ("--------------------------------------------")
        #print (GetInfoENdpoint)
        #print ("--------------------------------------------")
        #print ("CustomerId: ",CustomerId)
        #print ("StoreId: ",StoreId)
        #print ("EndpointId: ", EndpointId)
        #print ("HostName: ", HostName)
    return GetInfoENdpoint

#//////////////////////////////////////////////////////////////
# Data Test
# ////////////////////////////////////////////////////////////


# Get and Assign Version
GetVersion = GetVersionClient()
# Get ENdpint Info
GetInfoENdpoint = getinfo()
CustomerId = GetInfoENdpoint [0]
EndpointId = GetInfoENdpoint [2]
HostName = GetInfoENdpoint [3]
# Get Setting Info
GetSetting = GetEnpointSetting()
Nats = GetSetting [0]
Edge = GetSetting [1]


print ("--------------------------------------------")
print ("Version: ", GetVersion)
print ("--------------------------------------------")
print ("CustomerId: ",CustomerId)
print ("EndpointId: ", EndpointId)
print ("HostName: ", HostName)
print ("--------------------------------------------")
print ("Nats: ", Nats)
print ("Edge: ", Edge)

#input ()


#//////////////////////////////////////////////////////////////
# Process ID
# ////////////////////////////////////////////////////////////
# Obtener Process Id del Vsioncaptor
def GetProcessID ():
    while True:
        try:
            # Definimos el nombre de la Aplicacion que deseamos obtener el PID
            process_name = "Vsblty.VisionCaptor.exe"
            pid = None
            while pid == None:
                for proc in psutil.process_iter():
                    if process_name in proc.name():
                        pid = proc.pid
                #print (pid)
            return pid
        except ProcessLookupError:
            print ("Error al obtener ID, App not Run")

#//////////////////////////////////////////////////////////////
# Process CPU
# ////////////////////////////////////////////////////////////

# Get Process Id
PID = GetProcessID ()
print (PID)
# Obtener CPU

def getValueCpu (PID):
    test_list = []
    i = 0
    for i in range(10):
        p = psutil.Process(PID)
        p_cpu = p.cpu_percent(interval=1)/10
        #print (p_cpu)

        test_list.append(p_cpu)   
        x = float(sum(test_list))/len(test_list)
        Cpu = round(x, 2)
    #print ("CPU: ", Cpu)
    return Cpu

#//////////////////////////////////////////////////////////////
# Process RAM
# ////////////////////////////////////////////////////////////

# Obtener Ram en uso
def GetValueRam (PID):

    py = psutil.Process(PID)
    memoryUse = py.memory_info()[0]/2.**30  # memory use in GB...I think
    memoryUseValue = memoryUse*1000
    #print(memoryUse)
    return memoryUseValue

# //////////////////////////////////


# Get CPU Consuption
CPU = getValueCpu (PID)
# Get Ram Consuption
MemoryUse = GetValueRam (PID)

print ("--------------------------------------------")
print ("PID: ", PID)
print ("--------------------------------------------")
print ("CPU: ", CPU)
print ("--------------------------------------------")
print ("Ram: ", MemoryUse)

input ()



#////////////////////////////////////////
#
#///////////////////////////////////////
"""
i = 1
while i > 0:
    print ("--------------------------------------------")
    PID = GetProcessID ()
    Version = GetVersionClient()
   
    
    DateTest = datetime.datetime.now()
    EndpointSetting = GetEnpointSetting()
    Nats = EndpointSetting[0]
    edge = EndpointSetting[2]
    if edge == "true":
        service = "Edge"
    else:
        service = "Cloud"

    

    print ("Endpoint-Id: ", EndpointId)
    print ("HostName-Id: ", HostName)
    print ("Process-Id: ", PID)
    print ("Version: ", Version)
    print ("Cpu use: ", CPU)
    print ("Memory use: ", MemoryUse)
    print ("Date: ", DateTest)
    print ("Nats: ", Nats)
    print ("Edge: ", edge)
    print ("Service: ", service)
    i += 1 

    # Connect to the database
    connection = pymysql.connect(host='192.168.2.48',
        user='Qatest',
        password='Quito.2019',
        db='automatizacion',
        charset='utf8mb4',
        cursorclass=pymysql.cursors.DictCursor)

    try:
        with connection.cursor() as cursor:
    # Create a new record
                            
            sql = "INSERT INTO `performance` (`EndpointId`, `Hostname`, `PID`, `Version`, `service`,  `Nats`, `CpuUse`, `RamUse`, `date`) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)"
            cursor.execute(sql, (EndpointId, HostName, PID, Version, service, Nats, CPU, MemoryUse, DateTest))
            
    # connection is not autocommit by default. So you must commit to save
    # your changes.
            connection.commit()

    finally:
        connection.close()

    time.sleep(5)   
"""