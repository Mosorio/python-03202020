import json
import os, sys
from io import open
import datetime 
import time
import shutil
from shutil import rmtree
from os import makedirs
from os import remove

# Create Folder
# in this block the folders where the files are saved or moved are created
try:
    # change the destination path
    makedirs("C:/Json-Backup/")
    print ("creating folder Json-Backup")
except FileExistsError:
    print ("Json-Backup folder Exists")
#crear carpeta
try:
    # change the destination path
    makedirs("C:/Json-Backup/Entice/")
    print ("creating folder Entice")
except FileExistsError:
    print ("Entice folder Exists")
try:
    # change the destination path
    makedirs("C:/Json-Backup/Engage/")
    print ("creating folder Engage")
except FileExistsError:
    print ("Engage folder Exists")
try:
    # change the destination path
    makedirs("C:/Json-Backup/Interactive/")
    print ("creating folder Interactive")
except FileExistsError:
    print ("Interactive folder Exists")


print ("Destination folders created successfully, press enter to continue")
print ("Path C:/Json-Backup/")
input ()

#///////////////////////////////////////////
# GetFileJson
# Function to get all the files inside the usage folder
#//////////////////////////////////////////
def GetFileJson():
    #//////////////////////////////////
	# Verificar la cantidad de archivos que existen dentro del directorio
	# Indicamos la ruta de donde se quieren leer los archivos
    #path= "C:/ProgramData/Vsblty/Kiosk Framework/Usage/"
    print ("//////////////////////////////////////////////////////////////////////////")
    path= "C:/ProgramData/Vsblty/Kiosk Framework/Old_Usage/"
    print ("Original location of the files to process")
    print (path)
    print ("press enter to continue")
    input ()

    dirs = os.listdir(path)
    i=0
    for file in dirs:
        archivo = file
        rutacompleta = path + archivo
        FileType = rutacompleta.find("json")
        if  FileType > 1:
           
            try:
                i += 1
                print ("Item:", i)
                print ("File: ",rutacompleta)                
                #///////////////////////////////////////////
                with open(rutacompleta, ) as contenido:
                    datajson = json.load(contenido)

                    #encoded
                    data_string = json.dumps(datajson)

                    #Decoded
                    decoded = json.loads(data_string)

                    data_string = json.dumps(datajson)

                    # of each json file, we get the Type engagement property
                    engagementType = str(decoded["engagementType"])

                    #///////////////////////
                    print ("Engagement Type: ", engagementType)
                    print ("////////////////////////////////////////////////////////////////////////////////////////")
                    # according to the engagementType value we move to the folder
                    if int(engagementType) == 1:
                        # change the destination path
                        shutil.copy(rutacompleta, "C:/Json-Backup/Entice")
                        print ("file moved to Entice")
                    elif int(engagementType) == 2:
                        # change the destination path
                        shutil.copy(rutacompleta, "C:/Json-Backup/Engage")
                        print ("file moved to Engage")
                    elif int(engagementType) == 3:
                        # change the destination path
                        shutil.copy(rutacompleta, "C:/Json-Backup/Interactive")
                        print ("file moved to Interactive")

                        #time.sleep(.300)
                        #os.remove(rutacompleta) 
                        
            except KeyError as error:
                    print ("Error, json file does not contain engagementType property")

        else: 
            print("File is not of json type")


GetFileJson()

print ("----------------------------------------")
print ("------ End process------------------")
input()
input()

