import json
import os, sys
from io import open
import smtplib
import pymysql.cursors
import datetime 
import time
import uuid 
import shutil
from shutil import rmtree
from os import makedirs
from os import remove

#///////////////////////////////////////////
# GET-Version-Client
# Funcion para obtener la Version del Cliente, desde el archivo de Log
#//////////////////////////////////////////
def GetVersionClient():
    #//////////////////////////////////
	# Verificar la cantidad de archivos que existen dentro del directorio
	# Indicamos la ruta de donde se quieren leer los archivos
    path= "C:/ProgramData/Vsblty/Kiosk Framework/Usage/"
    dirs = os.listdir(path)
    #SearchVersion = "AppVersion"
    i=0
    for file in dirs:
        ruta = "C:/ProgramData/Vsblty/Kiosk Framework/Usage/"
        archivo = file
        rutacompleta = ruta + archivo
        FileType = rutacompleta.find("json")
        if  FileType > 1:
           
            try:
                i += 1
                print ("Item:", i)
                print ("File: ",rutacompleta)
                
                #///////////////////////////////////////////
                # GET-Endpoint-Setting
                # Funcion para obtener la Configuracion del Endpoint, desde el archivo de Globalvars.json
                #//////////////////////////////////////////
                
                #Iniciamos Lista que contendra toda la configuracion del Endpoint
                #EndpointSetting = []
                with open(rutacompleta) as contenido:
                    datajson = json.load(contenido)

                    #encoded
                    data_string = json.dumps(datajson)

                    #Decoded
                    decoded = json.loads(data_string)

                    data_string = json.dumps(datajson)

                    # Asignamos a una variable el resultado,
                    # En el Primer elemento ["List"], es el padre de donde queremos buscar o seleccionar el valor o propiedad 
                    # En el segundo elemento [0], es la propiedad que deseamos obtener
                    # el tercer elemeto ["Value"], es el valor de la propiedad
                    # Asignacion de Valores
                    #print ("**********************************")
                    engagementType = str(decoded["engagementType"])

                    #///////////////////////
                    # Asignamos configuracion a la Lista
                    #///////////////////////
                    #EndpointSetting = [ContentSwappedInterval,CameraHardwareName, IPAddress, IsPollingCamera, PollingCameraInterval, CollectAnalytics, DemograhicDataExpirationTime, EmailPublisherBioRecordExpiration, EmailPublisherIdentificationTriggerThreshold, EnableUseVizsafe, IsBodyDetectionTurnedOn, Sad, IsIdentitySearchTurnedOn, LiveEndpointData, NotificationEmailIdentifiacion, ObjectDetection, FaceDetectionAggressiveness, OpenVinoFace, FaceDetectionModel, PersonDetectionType, ReocurringVisitor]
                    #EndpointSetting = [engagementType]
                    #print (EndpointSetting)
                    print ("Engagement Type: ", engagementType)
                    print ("////////////////////////////////////////////////////////////////////////////////////////")
                    #return engagementType
                    if int(engagementType) == 1:
                        shutil.copy(rutacompleta, "C:/Users/Mijail QA/Documents/json")
                        print ("archivo movido")
                        time.sleep(.500) 


            except KeyError as error:
                    print ("Error, archivo json no contiene propiedad engagementType")

        else: 
            print("Archivo no es json")

        

    #return AppVersion

GetVersionClient()



input()
