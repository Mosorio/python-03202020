from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config ['SQLALCHEMY DATABASE_URI'] = 'mysql://scott:tiger@localhost/mydatabase'
app.config ['SQLALCHEMY TRACK MODIFICATIONS'] = False

db = SQLAlchemy(app)

class User(db.Model):
    hostname = db.Column(db.String(100))
    service  = db.Column(db.String(100))

    def __init__(self, hostname, service):
        self.hostname = hostname
        self.service = service

