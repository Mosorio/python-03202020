import json
import os, sys
from io import open
import smtplib
import pymysql.cursors
import datetime 
import time
import uuid 

#variables de Entorno
# Generamos Un GUID 
IdUnico = uuid.uuid4()
# Convertimos el GUID obtenido con "uuid.uuid4", en una cadena para poder guardar en la DB
# la Variable "GuidTest" define el Id de la Prueba Actual
GuidTest = str(IdUnico)


#-------------------------- Bloque 1 --------------
# Funcion para realizar busquedas dentro de los archivos Logs 
# Esta funcion recorre todos los archivos Logs busca las Lineas que contengan el valor del parametro "TextLogSearch"
# Recortando

def LogSearch(GuidTest, TextLogSearch):
    countLog = 0
    # Verificar la cantidad de archivos que existen dentro del directorio
    path= "C:/ProgramData/Vsblty/KingSalmon/"
    dirs = os.listdir(path)
    for file in dirs:
        ruta = "C:/ProgramData/Vsblty/KingSalmon/"
        archivo = file
        rutacompleta = ruta + archivo
        #print ("**********************************************************************")
        #print ("--- Read in file: ", archivo)
        #print ("--- Path: ", rutacompleta)
        #print ("**********************************************************************")
        #print ("\n")
        
        # La aplicaicon lee y guarada en la variable "archivo_texto" toda la informacion 
        archivo_texto=open (rutacompleta, "r")

        # Se lee Linea por linea
        lineas_texto=archivo_texto.readlines()

        #---------------- Inicio de Busqueda
        # Buscar segun el valor del parametro
        # Por cada linea se valida si contiene el Texto que fue eniado como parametro 
        for ClientLine in lineas_texto:
                if TextLogSearch in ClientLine:
                    ClientDate=ClientLine[:19]
                    LogInfo = ClientLine[38:]
                    #print ("Line Complet: ", StopClientLine)
                    #print ("Client Stop Time: ",StopClientDate)
                    #print ("--- File: ", archivo)
                    #print ("--- Path: ", rutacompleta)
                    #print ("\n")
                    countLog = countLog + 1
                    # la Lista continer
                    # countLog = veces en que un log fue encontrado
                    # TextLogSearch = parametro de Busqueda
                    # ClientDate = Fecha y hora de la linea encontrada
                    # archivo = Archivo donde se encontro o se obtuvo un match
                    # ClientLine = Linea completa con la cual se encontro un match
                    ReadFile  = [GuidTest, countLog, TextLogSearch, ClientDate, archivo, LogInfo]
                    #print ("Parametro: ", TextLogSearch)
                    print ("ReadFile[List]: ",ReadFile)
                    # Connect to the database
                    connection = pymysql.connect(host='localhost',
                        user='qatest',
                        password='Quito.2019',
                        db='tooltest',
                        charset='utf8mb4',
                        cursorclass=pymysql.cursors.DictCursor)

                    try:
                        with connection.cursor() as cursor:
                    # Create a new record
                    #sql = "INSERT INTO `error_log` (`ErrorInfo`, `TestNumer`, `ErrorNumero`) VALUES (%s, %s, %s)"
                    #cursor.execute(sql, (ErrorClient, 2, '32'))
                            
                            
                            #GuidTest = "85cea318-abfb-40dc-afe1-88a373522d04"
                            dateTest = "2019-10-23 20:37:40"
                            CustomerId = "bfe24339-135e-4bcf-8f7c-3a8059653139"
                            EndpointId = "3cf3d514-8374-443b-a74f-2be5bb4d7df1"
                            HostName = "DESKTOP-MOSORIO"
                            VersionClient = "3.19.1023.01"
                            StartClient = "2019-10-23 20:37:40"
                            StopClient = "2019-10-23 20:37:40"
                            RunTimeClient = "2019-10-23 20:37:40"
                            TagLog = "TAGTEST"
                            TestEnpoint = 22

                             
                            sql = "INSERT INTO `logdescription` (`GuidTest`, `DateTest`, `CustomerId`, `EndpointId`, `Hostname`, `TestEnpoint`, `VersionClient`, `StartClient`, `StopClient`, `RunTimeClient`, `LogId`, `TagLog`, `InfoLog`, `File`, `TextLogSearch`) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
                            cursor.execute(sql, (GuidTest, dateTest, CustomerId, EndpointId, HostName, TestEnpoint, VersionClient, StartClient, StopClient, RunTimeClient, countLog, TagLog, LogInfo, archivo, TextLogSearch))
                            #cursor.execute(sql, (ErrorClient, ErrorClient, ErrorClient, ErrorClient, ErrorClient, ErrorClient, ErrorClient))

                    # connection is not autocommit by default. So you must commit to save
                    # your changes.
                            connection.commit()

                    #with connection.cursor() as cursor:
                    # Read a single record
                    #sql = "SELECT `id`, `password` FROM `users` WHERE `email`=%s"
                    #cursor.execute(sql, ('webmaster@python.org',))
                    #result = cursor.fetchone()
                    #print(result)
                    finally:
                        connection.close()
    return ReadFile

# Buscamos en la funcion LogSearch segun parametros
# /////////////////////////
# asignamos Fecha de Incio del cliente
#LogSearch (GuidTest, "Starting App")
#StartClientDate = LogSearch(GuidTest, "Starting App")[2]
#StartClientTime=StartClientDate[11:19]
print ("**********************************************************************")

# Buscamos en la funcion LogSearch segun parametros
# /////////////////////////
# asignamos Fecha de Stop del cliente
#LogSearch (GuidTest, "Application exiting")
#StopClientDate = LogSearch(GuidTest, "Application exiting")[2]
#StopClientTime=StopClientDate[11:19]
print ("**********************************************************************")

# Convertimos las fechas de String a objetos DateTime
# /////////////////////////
# Formato para convertir Str en un objeto de Time
#format = "%Y-%m-%d %H:%M:%S"

# convirtiendo Str "StartClientDate" and "StopClientDate"
#datetime_objectStart = datetime.datetime.strptime(StartClientDate, format)
#datetime_objectStop = datetime.datetime.strptime(StopClientDate, format)
#ClientRunTime = datetime_objectStop - datetime_objectStart
print ("**********************************************************************")
#print ("Start Client: ", datetime_objectStart)
#print ("Stop Client: ", datetime_objectStop)
#print ("Client Runtime (H,M,S): ",ClientRunTime)
#print ("Deteciones por Minuto: 6")
#print ("Deteciones por Todo el Test: ")
#print ("**********************************************************************")
print ("\n")


#LogSearch ("ERROR")
print ("********************************** [CLOUD Detection] **********************************************")
#LogSearch (GuidTest, "[CLOUD Detection] Identified Person:")
#CLOUDDetection = LogSearch ("[FaceAnalysis][CLOUD Detection] Identified Person:")[4]
#print("LINE: ",CLOUDDetection)
print ("********************************** [EDGE Detection] ***********************************************")
LogSearch (GuidTest, "[EDGE Detection] Identified Person")
print ("********************************** [Client_SendCompleted DONE] ************************************")
LogSearch (GuidTest, "[Client_SendCompleted DONE]")
print ("********************************** Email Object Detection *****************************************")
LogSearch (GuidTest, "[Client_SendCompleted DONE] UserData:ObjectDetected:[RIFLE]")
#ObjectDetectedEmail = LogSearch("[Client_SendCompleted DONE] UserData:ObjectDetected:[RIFLE]")[4]
#print ("LINE: ",ObjectDetectedEmail)

