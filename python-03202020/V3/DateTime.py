import json
import os, sys
from io import open
import smtplib
import datetime 
import time

# Linea Log Starting and Application exiting
StartClientLine = "2019-10-23 10:10:00,384 [Thread:   1] INFO  Starting Application"
StopClientLine = "2019-10-23 11:32:00,801 [Thread:   1] INFO  Application exiting with code"
print ("Start Line: ", StartClientLine)
print (" Stop Line: ", StopClientLine)

#Date Test
StartClientDate=StartClientLine[:19]
StopClientDate=StopClientLine[:19]
# print ("Start Date: ", StartClientDate)
# print (" Stop Date: ", StopClientDate)

# Time Test
StartClientTime=StartClientLine[11:19]
StopClientTime=StopClientLine[11:19]
#print ("Start Time: ", StartClientTime)
#print (" Stop Time: ", StopClientTime)

# cambiar de cadena a fecha
print ("***************")

# Formato para convertir Str en un objeto de Time
format = "%Y-%m-%d %H:%M:%S"

# convirtiendo Str "StartClientDate" and "StopClientDate"
datetime_objectStart = datetime.datetime.strptime(StartClientDate, format)
datetime_objectStop = datetime.datetime.strptime(StopClientDate, format)
ClientRunTime = datetime_objectStop - datetime_objectStart


print ("Datetime: ", datetime_objectStart)
print ("Datetime: ", datetime_objectStop)
print ("Client runtime",ClientRunTime)


#print ("Hour: ", datetime_object.hour)
#print ("Minute: ", datetime_object.minute)
#print ("Second: ", datetime_object.second)
