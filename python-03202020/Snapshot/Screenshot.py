import pyautogui
import datetime
import uuid 
import time
import os, sys

Idfolder = uuid.uuid4()
GuidFolder = str(Idfolder)
try:
    os.mkdir("Capture")
except FileExistsError:
    print("Carpeta ya existe...")
       
folder =  "Capture/" + GuidFolder
os.mkdir(folder)

X=0
while X <=3000:
    IdUnico = uuid.uuid4()
    GuidTest = str(IdUnico)
    DateTest = datetime.datetime.now()
    
    file_path = folder + '/' + GuidTest + '.png'
    X += 1
    print("****************************************")
    print ("Screenshot", X)
    print(DateTest)
    print (GuidTest)
    myScreenshot = pyautogui.screenshot()
    myScreenshot.save(file_path)
    time.sleep(60)
   
